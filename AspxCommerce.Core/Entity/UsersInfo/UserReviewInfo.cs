﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;

namespace SanchiCommerce.Core
{
    [DataContract]
    [Serializable]
    public class UserReviewInfo
    {
        #region "Private Members"
        int _userID;
        string _userName;
        #endregion
        #region "Constructors"
        public UserReviewInfo()
        {
        }
        public UserReviewInfo(int userId, string userName)
        {
            this.UserID = userId;
            this.UserName = userName;
        }
        #endregion
        #region "Public Members"
        [DataMember]
        public int UserID
        {
            get { return _userID; }
            set { _userID = value; }
        }

        [DataMember]
        public string UserName
        {
            get { return _userName; }
            set { _userName = value; }
        }
    }
    #endregion

}
