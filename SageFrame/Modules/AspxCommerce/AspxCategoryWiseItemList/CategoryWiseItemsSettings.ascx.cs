﻿using System;
using System.Collections.Generic;
using AspxCommerce.CategoryWiseItemList;
using AspxCommerce.Core;
using SageFrame.Web;

public partial class Modules_AspxCommerce_AspxCategoryWiseItemsList_CategoryWiseItemsSettings : BaseAdministrationUserControl
{
    public string CatWiseItemModulePath;
    public int StoreID, PortalID, NoOfItemsInCategory;
    public string CultureName = string.Empty;
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (!IsPostBack)
            {
                StoreID = GetStoreID;
                PortalID = GetPortalID;
                CultureName = GetCurrentCultureName;
                CatWiseItemModulePath = ResolveUrl(this.AppRelativeTemplateSourceDirectory);
                IncludeLanguageJS();
                GetCatWiseSetting();
            }
        }
        catch (Exception ex)
        {
            ProcessException(ex);
        }

    }
    public void GetCatWiseSetting()
    {
        AspxCommonInfo aspxCommonObj = new AspxCommonInfo();
        aspxCommonObj.StoreID = StoreID;
        aspxCommonObj.PortalID = PortalID;       
        aspxCommonObj.CultureName = CultureName;
        AspxCatWiseItemController objCatWise = new AspxCatWiseItemController();
        CategoryWiseitemSettings catWiseSettingInfo =
            objCatWise.GetCategoryWiseItemSettings(aspxCommonObj);
        if (catWiseSettingInfo != null)
        {
            NoOfItemsInCategory = catWiseSettingInfo.NumberOfItemsInCategory;
        }
    }
}
