﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ItemsListByBrand.ascx.cs"
    Inherits="Modules_Admin_DetailsBrowse_ItemsListByBrand" %>

<script type="text/javascript">
    //<![CDATA[
    var BrandItemList = "";
    var arrCombination = [];
    IsExists = function (arr, val) {
        var isExist = false;
        for (var i = 0; i < arr.length; i++) {
            if (arr[i] == val) {
                isExist = true; break;
            }
        }
        return isExist;
    };
    function getObjects(obj, key) {

        var objects = [];
        for (var i in obj) {
            if (!obj.hasOwnProperty(i)) continue;
            if (typeof obj[i] == 'object') {
                objects.push(obj[i][key]);
            }

        }
        return objects;
    };
    CheckContains = function (checkon, toCheck) {
        var x = checkon.split('@');
        for (var i = 0; i < x.length; i++) {
            if (x[i] == toCheck) {
                return true;
            }
        }
        return false;
    };
    function getValByObjects(obj, key, key2, x, y, keyOfValue) {

        var value;
        for (var i in obj) {
            if (!obj.hasOwnProperty(i)) continue;
            if (typeof obj[i] == 'object') {
                if (obj[i][key] == x && obj[i][key2] == y) {
                    value = obj[i][keyOfValue];
                }
            }

        }
        return value;
    };
    CheckVariantCombination = function (costVIds, costValIds, currentCostVar, currentCostVal, ItemId) {
        //debugger;
        BrandItemList.info.IsCombinationMatched = false;
        var Combination = [];
        $.each(arrCombination, function (index, item) {

            if (ItemId == item.ItemID) {

                Combination.push(item);
            }

        });
        var cvcombinationList = getObjects(Combination, 'CombinationType');
        var cvValuecombinationList = getObjects(Combination, 'CombinationValues');
        for (var j = 0; j < cvcombinationList.length; j++) {
            if (BrandItemList.info.IsCombinationMatched == true)
                break;
            var matchedIndex = 0;
            var matchedValues = 0;
            if (cvcombinationList[j].length == costVIds.length) {
                var cb = costVIds.split('@');
                for (var id = 0; id < cb.length; id++) {
                    if (BrandItemList.info.IsCombinationMatched == true)
                        break;
                    var element = cb[id];
                    if (CheckContains(cvcombinationList[j], element)) {
                        matchedIndex++;
                        if (matchedIndex == cb.length) {
                            var cvb = costValIds.split('@');
                            for (var d = 0; d < cvb.length; d++) {
                                var element1 = cvb[d];
                                if (CheckContains(cvValuecombinationList[j], element1)) {
                                    matchedValues++;
                                }
                                if (matchedValues == cvb.length) {
                                    var combinationId = getValByObjects(Combination, 'CombinationType', 'CombinationValues', cvcombinationList[j], cvValuecombinationList[j], 'CombinationID');
                                    var modifiers = getModifiersByObjects(Combination, combinationId);
                                    BrandItemList.info.IsCombinationMatched = true;
                                    BrandItemList.info.CombinationID = combinationId;
                                    BrandItemList.info.IsPricePercentage = modifiers.IsPricePercentage;
                                    BrandItemList.info.PriceModifier = modifiers.Price;
                                    BrandItemList.info.Quantity = modifiers.Quantity;
                                    BrandItemList.info.IsWeightPercentage = modifiers.IsWeightPercentage;
                                    BrandItemList.info.WeightModifier = modifiers.Weight;
                                    break;
                                } else {
                                    BrandItemList.info.IsCombinationMatched = false;
                                    BrandItemList.info.CombinationID = 0;
                                    BrandItemList.info.IsPricePercentage = false;
                                    BrandItemList.info.PriceModifier = 0;
                                    BrandItemList.info.Quantity = 0;
                                    BrandItemList.info.IsWeightPercentage = false;
                                    BrandItemList.info.WeightModifier = 0;
                                }
                            }
                        } else {
                            BrandItemList.info.IsCombinationMatched = false;
                            BrandItemList.info.CombinationID = 0;
                            BrandItemList.info.IsPricePercentage = false;
                            BrandItemList.info.PriceModifier = 0;
                            BrandItemList.info.Quantity = 0;
                            BrandItemList.info.IsWeightPercentage = false;
                            BrandItemList.info.WeightModifier = 0;

                            var combinationIndex0 = cvcombinationList[j].split('@');
                            var combinationIndex01 = cvValuecombinationList[j].split('@');
                            for (var w = 0; w < combinationIndex0.length; w++) {
                                if (combinationIndex0[w] == currentCostVar) {
                                    if (combinationIndex01[w] == currentCostVal) {

                                        if (!IsExists(BrandItemList.info.AvailableCombination, cvValuecombinationList[j]))
                                            BrandItemList.info.AvailableCombination.push(cvValuecombinationList[j]);
                                    }
                                }
                            }
                        }
                    }

                }
            }
            else {
                var combinationIndex = cvcombinationList[j].split('@');
                var combinationIndex1 = cvValuecombinationList[j].split('@');
                for (var z = 0; z < combinationIndex.length; z++) {
                    if (combinationIndex[z] == currentCostVar) {
                        if (combinationIndex1[z] == currentCostVal) {
                            if (!IsExists(BrandItemList.info.AvailableCombination, cvValuecombinationList[j]))
                                BrandItemList.info.AvailableCombination.push(cvValuecombinationList[j]);
                        }
                    }
                }
            }
        }
        return BrandItemList.info;
    };
    $(function () {
       
        $(".sfLocale").localize({
            moduleKey: DetailsBrowse
        });
        var aspxCommonObj = {
            StoreID: SanchiCommerce.utils.GetStoreID(),
            PortalID: SanchiCommerce.utils.GetPortalID(),
            UserName: SanchiCommerce.utils.GetUserName(),
            CustomerID: SanchiCommerce.utils.GetCustomerID(),
            CultureName: SanchiCommerce.utils.GetCultureName(),
            SessionCode: SanchiCommerce.utils.GetSessionCode()
        };
        var variantQuery = '<%=variantQuery%>';
        var brandName = "<%=BrandName %>";
        var allowAddToCart = "<%=AllowAddToCart %>";
        var allowOutStockPurchase = "<%=AllowOutStockPurchase %>";
        var noImageBrandItemListByIdsPath = "<%=DefaultShoppingOptionImgPath %>";      
        var noOfItemsInRow = "<%=NoOfItemsInARow %>";
        var displaymode = "<%=ItemDisplayMode %>".toLowerCase();
        var currentPage = 0;
        var templateScriptArr = [];
        var rowTotal = 0;
        var Imagelist = '';
        var ImageType = {
            "Large": "Large",
            "Medium": "Medium",
            "Small": "Small"
        };
        BrandItemList = {
            config: {
                isPostBack: false,
                async: true,
                cache: true,
                type: 'POST',
                contentType: "application/json; charset=utf-8",
                data: '{}',
                dataType: 'json',
                baseURL: aspxservicePath + "AspxCoreHandler.ashx/",
                method: "",
                url: "",
                ajaxCallMode: "",
                error: ""
            },
            info: {
                IsCombinationMatched: false,
                AvailableCombination: [],
                CombinationID: 0,
                PriceModifier: '',
                IsPricePercentage: false,
                WeightModifier: '',
                IsWeightPercentage: false,
                Quantity: 0,
                MinCartQuantity: 0,
                MaxCartQuantity: 0

            },
            vars: {
                countCompareItems: "",
                itemSKU: SKU,
                itemQuantityInCart: "",
                userItemQuantityInCart: "",
                //userEmail: userEmail,
                //itemId: itemID,
                existReviewByUser: "",
                existReviewByIP: ""
            },
          
            ajaxCall: function(config) {
                $.ajax({
                    type: BrandItemList.config.type,
                    contentType: BrandItemList.config.contentType,
                    cache: BrandItemList.config.cache,
                    async: BrandItemList.config.async,
                    url: BrandItemList.config.url,
                    data: BrandItemList.config.data,
                    dataType: BrandItemList.config.dataType,
                    success: BrandItemList.config.ajaxCallMode,
                    error: BrandItemList.config.error
                });
            },
            //Send the list of images to the ImageResizer
            ResizeImageDynamically: function (Imagelist,Type,imgCatType) {
                BrandItemList.config.method = "DynamicImageResizer";
                BrandItemList.config.url = aspxservicePath + "AspxImageResizerHandler.ashx/" + this.config.method;
                BrandItemList.config.data = JSON2.stringify({ imgCollection: Imagelist, type: Type, imageCatType: imgCatType, aspxCommonObj: aspxCommonObj });
                BrandItemList.config.async = false;
                BrandItemList.config.ajaxCallMode = BrandItemList.ResizeImageSuccess;
                BrandItemList.ajaxCall(BrandItemList.config);

            },
            ResizeImageSuccess: function () {
            },
            BindBrandItemsResult: function (msg) {
                var length = msg.d.length;
                if (length > 0) {
                    rowTotal = msg.d[0].RowTotal;
                    $("#divBrandItemViewOptions").show();
                    $("#divBrandPageNumber").show();
                    Imagelist = '';
                    var item;
                    for (var index = 0; index < length; index++) {
                        item = msg.d[index];
                        if (item.BaseImage != "") {
                            Imagelist += item.BaseImage + ';';
                        }
                    };
                }
                BrandItemList.ResizeImageDynamically(Imagelist, ImageType.Medium,"Item");
                BindTemplateDetails('divBrandSearchResult', 'divBrandItemViewOptions', 'divBrandViewAs', 'ddlBrandViewAs', 'ddlBrandSortBy', 'divBrandPageNumber', 'BrandPagination', 'ddlBrandPageSize', currentPage, msg, BrandItemList.BindBrandResultItems, 'BrandItemList',allowAddToCart, allowOutStockPurchase, noImageBrandItemListByIdsPath, noOfItemsInRow, displaymode, templateScriptArr);
            },

            GetBrandLoadErrorMsg: function() {
                csscody.error("<h2>" + getLocale(DetailsBrowse, "Error Message") + "</h2><p>" + getLocale(Brand, "Sorry, Failed to load brand items results!") + "</p>");
            },
            GetBrandItemsByBrandID: function () {
                //debugger;
                BrandItemList.config.method = "GetBrandDetailByBrandID";
                BrandItemList.config.url = BrandItemList.config.baseURL + BrandItemList.config.method;
                BrandItemList.config.async = false;
                BrandItemList.config.data = JSON2.stringify({ brandName: brandName, aspxCommonObj: aspxCommonObj });
                BrandItemList.config.ajaxCallMode = BrandItemList.BindBrandItemsDetailSucess;
                                BrandItemList.ajaxCall(BrandItemList.config);
            },
            BindBrandItemsDetailSucess: function(data) {
                var element = '';
                if (data.d.length > 0) {
                    $.each(data.d, function (index, value) {
                        if (value.BrandImageUrl != "") {
                            var imgUrlSegments = value.BrandImageUrl.split('/');
                            var imgToBeAdded = imgUrlSegments[imgUrlSegments.length - 1] + ';';
                            Imagelist += imgToBeAdded;
                        }
                    });
                    BrandItemList.ResizeImageDynamically(Imagelist, ImageType.Medium,"Brand");
                    $.each(data.d, function(index, value) {
                        $(".sfBrandHeaderTitle").html(getLocale(DetailsBrowse, "Welcome to the ") + value.BrandName + getLocale(DetailsBrowse, " Store"));
                        element += '<div class="sfBrandLogo"><img src="' + aspxRootPath + value.BrandImageUrl.replace("uploads", "uploads/Medium") + '" title="' + value.BrandName + '" alt="' + value.BrandName + '"></div>';
                        element += '<div class="sfBrandDescription">' + Encoder.htmlDecode(value.BrandDescription) + '</div><div class="clear"></div>';
                      
                    });
                    $("#divBrandHeader").append(element);
                }
            },
            BindBrandResultItems: function (offset, limit, currentpage1, sortBy) {
                //debugger;
                var attribute = $.cookie("cityname") != null ? $.cookie("cityname").replace('%40', '@') : null;//added by jyoti for citywise
                currentPage = currentpage1;
                BrandItemList.config.method = "GetBrandItemsByBrandID";
                BrandItemList.config.url = BrandItemList.config.baseURL + BrandItemList.config.method;
                BrandItemList.config.data = JSON2.stringify({ offset: offset, limit: limit, brandName: brandName, SortBy: sortBy, rowTotal: rowTotal, aspxCommonObj: aspxCommonObj, attribute: attribute });
                BrandItemList.config.ajaxCallMode = BrandItemList.BindBrandItemsResult;
                BrandItemList.config.error = BrandItemList.GetBrandLoadErrorMsg;
                BrandItemList.ajaxCall(BrandItemList.config);
            },
            BrandItemsHideAll: function() {              
                $("#divBrandPageNumber").hide();
                $("#divBrandSearchResult").hide();
            },
            BindCostVariant: function (data, itemSKU, ItemID) {
                //debugger;
                SKU = itemSKU;
                ItemId = ItemID;
                if (data != null) {
                    var CostVariant = '';
                    var variantValue = [];
                    $.each(data, function (index, item) {
                        if (index != "clean") {
                            var CostVariantID = data[index].split(',')[0];
                            var CostVariantName = data[index].split(',')[1];
                            var InputTypeID = data[index].split(',')[2];
                            var CostVariantsValueID = data[index].split(',')[3];
                            var CostVariantsValueName = data[index].split(',')[4];
                            if (CostVariant.indexOf(CostVariantID) == -1) {
                                CostVariant += CostVariantID;
                                variantId.push(CostVariantID);
                                var addSpan = '';
                                addSpan += '<div id="div_' + CostVariantID + '_' + ItemID + '" class="cssClassHalfColumn_' + ItemID + '">';
                                //addSpan += '<span id="spn_' + CostVariantID + '_'+ ItemID +'" ><b>' + CostVariantName + ':</b> ' + '</span>';
                                //addSpan += '<span class="spn_Close_'+ ItemID +'"><a href="#"><img class="imgDelete" src="' + aspxTemplateFolderPath + '/images/admin/uncheck.png" title="' + getLocale(DetailsBrowse, "Don\'t use this option") + '"" alt="' + getLocale(DetailsBrowse, "Don\'t use this option") + '"/></a></span>';
                                $('#divCostVariant_' + ItemID + '').append(addSpan);
                                addSpan += '</div>';
                            }
                            var items = {
                                CostVariantID: CostVariantID,
                                CostVariantName: CostVariantName,
                                CostVariantsPriceValue: null,
                                CostVariantsValueID: CostVariantsValueID,
                                CostVariantsValueName: CostVariantsValueName,
                                CostVariantsWeightValue: null,
                                InputTypeID: InputTypeID,
                                IsOutOfStock: false,
                                IsPriceInPercentage: null,
                                IsWeightInPercentage: null
                            }
                            var valueID = '';
                            var itemCostValueName = '';
                            if (CostVariantsValueID != -1) {
                                if (InputTypeID == 5 || InputTypeID == 6) {
                                    if ($('#controlCostVariant_' + CostVariantID + '_' + ItemId + '').length == 0) {
                                        itemCostValueName += '<span class="sfListmenu" id="subDiv' + CostVariantID + '_' + ItemId + '">';
                                        valueID = 'controlCostVariant_' + CostVariantID + '_' + ItemId;
                                        itemCostValueName += BrandItemList.CreateControl(items, valueID, false);
                                        itemCostValueName += "</span>";
                                        $('#div_' + CostVariantID + '_' + ItemId + '').append(itemCostValueName);
                                    }
                                    if (!IsExists(variantValue, CostVariantsValueID)) {
                                        variantValue.push(CostVariantsValueID);
                                        optionValues = BrandItemList.BindInsideControl(items, valueID);
                                        $('#controlCostVariant_' + CostVariantID + '_' + ItemId + '').append(optionValues);
                                    }
                                    $('#controlCostVariant_' + CostVariantID + '_' + ItemId + ' option:first-child').prop("selected", "selected");
                                }
                                else {
                                    if ($('#subDiv' + CostVariantID + '_' + ItemId + '').length == 0) {
                                        itemCostValueName += '<span class="cssClassRadio" id="subDiv' + CostVariantID + '_' + ItemId + '">';
                                        valueID = 'controlCostVariant_' + CostVariantID + '_' + ItemId;
                                        itemCostValueName += BrandItemList.CreateControl(items, valueID, true);
                                        itemCostValueName += "</span>";
                                        $('#div_' + CostVariantID + '').append(itemCostValueName);
                                    } else {
                                        valueID = 'controlCostVariant_' + CostVariantID + '_' + ItemId;
                                        itemCostValueName += BrandItemList.CreateControl(items, valueID, false);
                                        $('#subDiv' + CostVariantID + '_' + ItemId + '').append(itemCostValueName);
                                    }
                                }
                            }
                        }
                    });

                    $('#divCostVariant_' + ItemID + '').append('<div class="cssClassClear"></div>');
                    if (!$('#divCostVariant_' + ItemID + '').html().trim()) {
                        $('#divCostVariant_' + ItemID + '').removeClass("cssClassCostVariant");
                    } else {
                        $('#divCostVariant_' + ItemID + '').addClass("cssClassCostVariant");
                    }
                    if ($.session("ItemCostVariantData") != undefined) {
                        $.each(arrCostVariants, function (i, variant) {
                            var itemColl = $('#divCostVariant_' + ItemID + '').find("[Variantname=" + variant + "]");
                            if ($(itemColl).is("input[type='checkbox'] ,input[type='radio']")) {
                                $('#divCostVariant_' + ItemID + '').find("input:checkbox").removeAttr("checked");
                                $(itemColl).prop("checked", "checked");
                            } else if ($(itemColl).is('select>option')) {
                                $('#divCostVariant_' + ItemID + '').find("select>option").removeAttr("selected");
                                $(itemColl).prop("selected", "selected");
                            }

                        });
                        $.session("ItemCostVariantData", 'empty');
                    }
                    $('#divCostVariant_' + ItemID + ' select,#divCostVariant_' + ItemID + ' input[type=radio],#divCostVariant_' + ItemID + ' input[type=checkbox]').unbind().bind("change", function () {
                        //debugger;
                        checkAvailibility(this);
                    });

                    $('#divCostVariant_' + ItemID + '').on("click", ".spn_Close", function () {

                        $(this).next('span:first').find(" input[type=radio]").removeAttr('checked');
                        if ($(this).next('span:first').find("select").find("option[value=0]").length == 0) {
                            var options = $(this).next('span:first').find("select").html();
                            var noOption = "<option value=0 >" + getLocale(DetailsBrowse, "Not required") + "</option>";
                            $(this).next('span:first').find("select").html(noOption + options);
                        } else {
                            $(this).next('span:first').find("select").find("option[value=0]").prop('selected', 'selected');
                        }
                        checkAvailibility(null);
                    });
                    $('.cssClassDropDownItem').MakeFancyItemDropDown();
                    setTimeout(function () {

                        BrandItemList.LoadCostVariantCombination(itemSKU);
                        BrandItemList.variantCheckQuery();
                        selectFirstcombination(ItemID);
                    }, 200);

                }
                else {
                    BrandItemList.LoadCostVariantCombination(itemSKU);
                }
            },
            CreateControl: function (item, controlID, isChecked) {

                var controlElement = '';
                var costPriceValue = item.CostVariantsPriceValue;
                var weightValue = item.CostVariantsWeightValue;
                if (item.InputTypeID == 5) {
                    controlElement = "<select id='" + controlID + "' multiple></select>";
                } else if (item.InputTypeID == 6) {
                    controlElement = "<select id='" + controlID + "'></select>";
                } else if (item.InputTypeID == 9 || item.InputTypeID == 10) {
                    controlElement = "<label><input  name='" + controlID + "' type='radio' checked='checked' value='" + item.CostVariantsValueID + "'><span>" + item.CostVariantsValueName + "</span></label>";

                } else if (item.InputTypeID == 11 || item.InputTypeID == 12) {
                    controlElement = "<input  name='" + controlID + "' type='radio' checked='checked' value='" + item.CostVariantsValueID + "'><label>" + item.CostVariantsValueName + "</label></br>";

                }
                return controlElement;
            },
            BindInsideControl: function (item, controlID) {
                var optionValues = '';
                var costPriceValue = item.CostVariantsPriceValue;
                var weightValue = item.CostVariantsWeightValue;
                if (item.InputTypeID == 5) {
                    optionValues = "<option value=" + item.CostVariantsValueID + ">" + item.CostVariantsValueName + "</option>";

                } else if (item.InputTypeID == 6) {
                    optionValues = "<option value=" + item.CostVariantsValueID + ">" + item.CostVariantsValueName + "</option>";

                }
                return optionValues;
            },
            LoadCostVariantCombination: function (itemSKU) {

                var param = JSON2.stringify({ itemSku: itemSKU, aspxCommonObj: aspxCommonObj });
                this.config.method = "AspxCoreHandler.ashx/GetCostVariantCombinationbyItemSku";
                this.config.url = this.config.baseURL + this.config.method;
                this.config.data = param;
                this.config.ajaxCallMode = BrandItemList.BindCostVariantCombination;
                this.config.async = false;
                this.ajaxCall(this.config);
            },
            BindCostVariant: function (data, itemSKU, ItemID) {
                //debugger;
                SKU = itemSKU;
                ItemId = ItemID;
                if (data != null) {
                    var CostVariant = '';
                    var variantValue = [];
                    $.each(data, function (index, item) {
                        if (index != "clean") {
                            var CostVariantID = data[index].split(',')[0];
                            var CostVariantName = data[index].split(',')[1];
                            var InputTypeID = data[index].split(',')[2];
                            var CostVariantsValueID = data[index].split(',')[3];
                            var CostVariantsValueName = data[index].split(',')[4];
                            if (CostVariant.indexOf(CostVariantID) == -1) {
                                CostVariant += CostVariantID;
                                variantId.push(CostVariantID);
                                var addSpan = '';
                                addSpan += '<div id="div_' + CostVariantID + '_' + ItemID + '" class="cssClassHalfColumn_' + ItemID + '">';
                                //addSpan += '<span id="spn_' + CostVariantID + '_'+ ItemID +'" ><b>' + CostVariantName + ':</b> ' + '</span>';
                                //addSpan += '<span class="spn_Close_'+ ItemID +'"><a href="#"><img class="imgDelete" src="' + aspxTemplateFolderPath + '/images/admin/uncheck.png" title="' + getLocale(DetailsBrowse, "Don\'t use this option") + '"" alt="' + getLocale(DetailsBrowse, "Don\'t use this option") + '"/></a></span>';
                                $('#divCostVariant_' + ItemID + '').append(addSpan);
                                addSpan += '</div>';
                            }
                            var items = {
                                CostVariantID: CostVariantID,
                                CostVariantName: CostVariantName,
                                CostVariantsPriceValue: null,
                                CostVariantsValueID: CostVariantsValueID,
                                CostVariantsValueName: CostVariantsValueName,
                                CostVariantsWeightValue: null,
                                InputTypeID: InputTypeID,
                                IsOutOfStock: false,
                                IsPriceInPercentage: null,
                                IsWeightInPercentage: null
                            }
                            var valueID = '';
                            var itemCostValueName = '';
                            if (CostVariantsValueID != -1) {
                                if (InputTypeID == 5 || InputTypeID == 6) {
                                    if ($('#controlCostVariant_' + CostVariantID + '_' + ItemId + '').length == 0) {
                                        itemCostValueName += '<span class="sfListmenu" id="subDiv' + CostVariantID + '_' + ItemId + '">';
                                        valueID = 'controlCostVariant_' + CostVariantID + '_' + ItemId;
                                        itemCostValueName += BrandItemList.CreateControl(items, valueID, false);
                                        itemCostValueName += "</span>";
                                        $('#div_' + CostVariantID + '_' + ItemId + '').append(itemCostValueName);
                                    }
                                    if (!IsExists(variantValue, CostVariantsValueID)) {
                                        variantValue.push(CostVariantsValueID);
                                        optionValues = BrandItemList.BindInsideControl(items, valueID);
                                        $('#controlCostVariant_' + CostVariantID + '_' + ItemId + '').append(optionValues);
                                    }
                                    $('#controlCostVariant_' + CostVariantID + '_' + ItemId + ' option:first-child').prop("selected", "selected");
                                }
                                else {
                                    if ($('#subDiv' + CostVariantID + '_' + ItemId + '').length == 0) {
                                        itemCostValueName += '<span class="cssClassRadio" id="subDiv' + CostVariantID + '_' + ItemId + '">';
                                        valueID = 'controlCostVariant_' + CostVariantID + '_' + ItemId;
                                        itemCostValueName += BrandItemList.CreateControl(items, valueID, true);
                                        itemCostValueName += "</span>";
                                        $('#div_' + CostVariantID + '').append(itemCostValueName);
                                    } else {
                                        valueID = 'controlCostVariant_' + CostVariantID + '_' + ItemId;
                                        itemCostValueName += BrandItemList.CreateControl(items, valueID, false);
                                        $('#subDiv' + CostVariantID + '_' + ItemId + '').append(itemCostValueName);
                                    }
                                }
                            }
                        }
                    });

                    $('#divCostVariant_' + ItemID + '').append('<div class="cssClassClear"></div>');
                    if (!$('#divCostVariant_' + ItemID + '').html().trim()) {
                        $('#divCostVariant_' + ItemID + '').removeClass("cssClassCostVariant");
                    } else {
                        $('#divCostVariant_' + ItemID + '').addClass("cssClassCostVariant");
                    }
                    if ($.session("ItemCostVariantData") != undefined) {
                        $.each(arrCostVariants, function (i, variant) {
                            var itemColl = $('#divCostVariant_' + ItemID + '').find("[Variantname=" + variant + "]");
                            if ($(itemColl).is("input[type='checkbox'] ,input[type='radio']")) {
                                $('#divCostVariant_' + ItemID + '').find("input:checkbox").removeAttr("checked");
                                $(itemColl).prop("checked", "checked");
                            } else if ($(itemColl).is('select>option')) {
                                $('#divCostVariant_' + ItemID + '').find("select>option").removeAttr("selected");
                                $(itemColl).prop("selected", "selected");
                            }

                        });
                        $.session("ItemCostVariantData", 'empty');
                    }
                    $('#divCostVariant_' + ItemID + ' select,#divCostVariant_' + ItemID + ' input[type=radio],#divCostVariant_' + ItemID + ' input[type=checkbox]').unbind().bind("change", function () {

                        checkAvailibility(this);
                    });

                    $('#divCostVariant_' + ItemID + '').on("click", ".spn_Close", function () {

                        $(this).next('span:first').find(" input[type=radio]").removeAttr('checked');
                        if ($(this).next('span:first').find("select").find("option[value=0]").length == 0) {
                            var options = $(this).next('span:first').find("select").html();
                            var noOption = "<option value=0 >" + getLocale(DetailsBrowse, "Not required") + "</option>";
                            $(this).next('span:first').find("select").html(noOption + options);
                        } else {
                            $(this).next('span:first').find("select").find("option[value=0]").prop('selected', 'selected');
                        }
                        checkAvailibility(null);
                    });
                    $('.cssClassDropDownItem').MakeFancyItemDropDown();
                    setTimeout(function () {

                        BrandItemList.LoadCostVariantCombination(itemSKU);
                        BrandItemList.variantCheckQuery();
                        selectFirstcombination(ItemID);
                    }, 200);

                }
                else {
                    BrandItemList.LoadCostVariantCombination(itemSKU);
                }
            },
            variantCheckQuery: function () {
                //debugger;
                if (variantQuery != null && variantQuery != '') {
                    var variantIds = variantQuery.split('@');
                    var elem = null;
                    $.each(variantIds, function (index, value) {
                        if ($('#divCostVariant_' + ItemId + '').find(".sfListmenu").find('select option[value=' + value + ']').parents().attr("id") != undefined) {
                            $('#divCostVariant_' + ItemId + '').find(".sfListmenu").find('select option[value=' + value + ']').prop("selected", "selected");
                            var id = $('#divCostVariant_' + ItemId + '').find(".sfListmenu").find('select option[value=' + value + ']').parents().attr("id");
                            id = parseFloat(id.substring(id.lastIndexOf('_') + 1, id.length));
                            if (variantId.indexOf(id) != -1) {
                                variantId.splice(variantId.indexOf(id), 1);
                            }
                            elem = $('#divCostVariant_' + ItemId + '').find(".sfListmenu").find('select option[value=' + value + ']');
                        }
                        if ($('#divCostVariant_' + ItemId + '').find('input:radio[value=' + value + ']').attr("name") != undefined) {
                            $('#divCostVariant_' + ItemId + '').find('input:radio[value=' + value + ']').prop("checked", true);
                            var name = $('#divCostVariant_' + ItemId + '').find('input:radio[value=' + value + ']').attr("name");
                            name = parseFloat(name.substring(name.lastIndexOf('_') + 1, name.length));
                            if (variantId.indexOf(name) != -1) {
                                variantId.splice(variantId.indexOf(name), 1);
                            }
                            checkAvailibility($('#divCostVariant_' + ItemId + '').find('input:radio[value=' + value + ']'));
                            elem = $('#divCostVariant_' + ItemId + '').find('input:radio[value=' + value + ']');
                        }
                    });
                    $.each(variantId, function (index, value) {

                        if ($("#controlCostVariant_" + value).parents().is('.sfListmenu')) {
                            $("#controlCostVariant_" + value).prepend("<option value=0 >" + getLocale(DetailsBrowse, "Not required") + "</option>");
                            $("#controlCostVariant_" + value).find("option[value=0]").prop('selected', 'selected');
                            elem = null;
                        }
                        else {
                            $('.cssClassRadio input[name="controlCostVariant_' + value + '"]').removeAttr('checked');
                            elem = null;
                        }
                    });

                    checkAvailibility(elem);
                    if (BrandItemList.info.IsCombinationMatched == false)
                        selectFirstcombination(ItemId);
                }

            },
            BindCostVariantCombination: function (msg) {

                $.each(msg.d, function (index, item) {
                    var CostVariantCombination = {
                        CombinationID: 0,
                        CombinationType: "",
                        CombinationValues: "",
                        CombinationPriceModifier: "",
                        CombinationPriceModifierType: "",
                        CombinationWeightModifier: "",
                        CombinationWeightModifierType: "",
                        CombinationQuantity: 0,
                        ImageFile: "",
                        ItemID: ""
                    };
                    CostVariantCombination = item;
                    arrCombination.push(CostVariantCombination);
                });
                selectFirstcombination(ItemId);
            },
            AddToMyCart: function (itemTypeId, ItmID) {
                //debugger;
                if (itemTypeId == 3) {
                    var giftCardDetail = {
                        Price: $("#hdnPrice_" + ItemId + "").val(),
                        GiftCardTypeId: parseFloat($("input[name=giftcard-type]:checked").val()),
                        GiftCardCode: '',
                        GraphicThemeId: parseFloat($(".jcarousel-skin ul li a.selected").attr('data-id')),
                        SenderName: $.trim($("#txtgc_senerName").val()),
                        SenderEmail: $.trim($("#txtgc_senerEmail").val()),
                        RecipientName: $.trim($("#txtgc_recieverName").val()),
                        RecipientEmail: $.trim($("#txtgc_recieverEmail").val()),
                        Messege: $.trim($("#txtgc_messege").val())
                    };


                    if (parseFloat($("input[name=giftcard-type]:checked").val()) == 2) {

                    } else {
                        if ($.trim($("#txtgc_recieverName").val()) == "" ||
                            $.trim($("#txtgc_recieverEmail").val()) == "") {

                            csscody.alert('<h2>' + getLocale(DetailsBrowse, 'Information Alert') + '</h2><p>' + getLocale(DetailsBrowse, 'Please fill valid required data!') + '</p>');
                            return false;

                        } else {
                            if (!/^[\w\-\.\+]+\@[a-zA-Z0-9\.\-]+\.[a-zA-z0-9]{2,4}$/.test(giftCardDetail.RecipientEmail)) {
                                csscody.alert("<h2>" + getLocale(DetailsBrowse, "Information Alert") + "</h2><p>" + getLocale(DetailsBrowse, "Please fill valid email address!") + "</p>");
                                return false;
                            }

                        }
                    }

                    if (giftCardDetail.SenderName != "" || giftCardDetail.SenderEmail != "") {
                        if (!/^[\w\-\.\+]+\@[a-zA-Z0-9\.\-]+\.[a-zA-z0-9]{2,4}$/.test(giftCardDetail.SenderEmail)) {
                            csscody.alert("<h2>" + getLocale(DetailsBrowse, "Information Alert") + "</h2><p>" + getLocale(DetailsBrowse, "Please fill valid email address!") + "</p>");
                            return false;
                        }
                        var AddItemToCartObj = {
                            ItemID: itemId,
                            Price: $("#hdnPrice").val(),
                            Weight: 0,
                            Quantity: 1,
                            CostVariantIDs: '0@',
                            IsGiftCard: true,
                            IsKitItem: false
                        };
                        var paramz = {
                            aspxCommonObj: aspxCommonObj(),
                            AddItemToCartObj: AddItemToCartObj,
                            giftCardDetail: giftCardDetail,
                            kitInfo: {}
                        };
                        var dataz = JSON2.stringify(paramz);
                        this.config.method = "AspxCommonHandler.ashx/AddItemstoCartFromDetail";
                        this.config.url = this.config.baseURL + this.config.method;
                        this.config.data = dataz;
                        this.config.ajaxCallMode = BrandItemList.AddItemstoCartFromDetail;
                        this.config.oncomplete = 20;
                        this.config.error = BrandItemList.GetAddToCartErrorMsg;
                        this.ajaxCall(this.config);
                    }
                    else {
                        csscody.alert('<h2>' + getLocale(DetailsBrowse, "Information Alert") + "</h2><p>" + getLocale(DetailsBrowse, "Please fill valid required data!") + "</p>");
                        return false;
                    }
                }
                else if (itemTypeId == 6) {
                    //debugger;
                    var kitinfo = BrandItemList.Kit.GetKitInfo();
                    if (kitinfo.Data.length == 0) {
                        csscody.alert('<h2>' + getLocale(DetailsBrowse, "Information Alert") + "</h2><p>" + getLocale(DetailsBrowse, "Please choose valid configuration!") + "</p>");
                        return false;
                    }
                    var AddItemToCartObj = {
                        ItemID: itemId,
                        Price: kitinfo.Price,
                        Weight: kitinfo.Weight,
                        Quantity: $.trim($("#txtQty_" + itemId + "").val()),
                        CostVariantIDs: '0@',
                        IsGiftCard: false,
                        IsKitItem: true
                    };
                    var paramz = {
                        aspxCommonObj: aspxCommonObj(),
                        AddItemToCartObj: AddItemToCartObj,
                        giftCardDetail: {},
                        kitInfo: kitinfo
                    };
                    var itemsCartInfo = BrandItemList.CheckItemQuantityInCart(itemId, '0@');
                    var itemQuantityInCart = itemsCartInfo.ItemQuantityInCart;
                    var userItemQuantityInCart = itemsCartInfo.UserItemQuantityInCart;

                    if (itemQuantityInCart != -1) {
                        if (allowAddToCart.toLowerCase() == 'true') {
                            if (allowOutStockPurchase.toLowerCase() == 'false') {
                                if ((eval($("#txtQty_" + itemId + "").val()) + eval(userItemQuantityInCart)) < eval(BrandItemList.info.MinCartQuantity)) {
                                    csscody.alert("<h2>" + getLocale(DetailsBrowse, 'Information Alert') + '</h2><p>' + getLocale(AspxItemDeDetailsBrowsetails, 'The requested quantity for this item is not valid') + "</p>");
                                    return false;
                                }
                                else if ((eval($("#txtQty_" + itemId + "").val()) + eval(userItemQuantityInCart)) > eval(BrandItemList.info.MaxCartQuantity)) {
                                    csscody.alert("<h2>" + getLocale(DetailsBrowse, 'Information Alert') + '</h2><p>' + getLocale(DetailsBrowse, 'The requested quantity for this item is not valid') + "</p>");
                                    return false;
                                }
                                else if (BrandItemList.info.Quantity <= 0) {
                                    csscody.alert("<h2>" + getLocale(DetailsBrowse, 'Information Alert') + '</h2><p>' + getLocale(DetailsBrowse, 'This product is currently Out Of Stock!') + "</p>");
                                    return false;
                                } else {
                                    if ((eval($.trim($("#txtQty_" + itemId + "").val())) + eval(itemQuantityInCart)) > eval(BrandItemList.info.Quantity)) {
                                        csscody.alert("<h2>" + getLocale(DetailsBrowse, 'Information Alert') + '</h2><p>' + getLocale(DetailsBrowse, 'This product is currently Out Of Stock!') + "</p>");
                                        return false;
                                    }
                                }
                            }
                        }
                        else {
                            $("#btnAddToMyCart").parent('label').remove();
                        }
                    }
                    var dataz = JSON2.stringify(paramz);
                    this.config.method = "AspxCommonHandler.ashx/AddItemstoCartFromDetail";
                    this.config.url = this.config.baseURL + this.config.method;
                    this.config.data = dataz;
                    this.config.ajaxCallMode = BrandItemList.AddItemstoCartFromDetail;
                    this.config.oncomplete = 20;
                    this.config.error = BrandItemList.GetAddToCartErrorMsg;
                    this.ajaxCall(this.config);
                }
                else {
                    checkAvailibility($("#divCostVariant_" + ItmID + " select"));
                    if (BrandItemList.info.IsCombinationMatched) {
                        //debugger;
                        if ($.trim($("#txtQty_" + ItmID + "").val()) == "" || $.trim($("#txtQty_" + ItmID + "").val()) <= 0) {
                            csscody.alert('<h2>' + getLocale(DetailsBrowse, 'Information Alert') + '</h2><p>' + getLocale(DetailsBrowse, 'Invalid quantity.') + '</p>');
                            return false;
                        }
                        var itemPrice = $("#hdnPrice_" + ItmID + "").val();
                        var itemQuantity = $.trim($("#txtQty_" + ItmID + "").val());
                        var Qty = $("#hdnQuantity_" + ItmID + "").val();
                        var itemCostVariantIDs = [];
                        var weightWithVariant = 0;
                        var totalWeightVariant = 0;
                        var costVariantPrice = 0;
                        if (!$("#divCostVariant_" + ItmID + "").html().trim()) {
                            itemCostVariantIDs.push(0);
                        } else {
                            $("#divCostVariant_" + ItmID + " select option:selected").each(function () {
                                if ($(this).val() != 0) {
                                    itemCostVariantIDs.push($(this).val());
                                } else {
                                }
                            });
                            $("#divCostVariant_" + ItmID + " input[type=radio]:checked").each(function () {
                                if ($(this).val() != 0) {
                                    itemCostVariantIDs.push($(this).val());
                                } else {
                                }
                            });

                            $("#divCostVariant_" + ItmID + " input[type=checkbox]:checked").each(function () {
                                if ($(this).val() != 0) {
                                    itemCostVariantIDs.push($(this).val());
                                } else { }
                            });

                        }

                        var itemsCartInfo = BrandItemList.CheckItemQuantityInCart(ItmID, itemCostVariantIDs.join('@') + '@');
                        var itemQuantityInCart = itemsCartInfo.ItemQuantityInCart;
                        var userItemQuantityInCart = itemsCartInfo.UserItemQuantityInCart;

                        if (itemQuantityInCart != -1) {
                            if (allowAddToCart.toLowerCase() == 'true') {
                                if (allowOutStockPurchase.toLowerCase() == 'false') {
                                    //if ((eval($("#txtQty_" + ItmID + "").val()) + eval(userItemQuantityInCart)) < eval(categoryDetails.info.MinCartQuantity)) {
                                    //    csscody.alert("<h2>" + getLocale(DetailsBrowse, 'Information Alert') + '</h2><p>' + getLocale(DetailsBrowse, 'The requested quantity for this item is not valid') + "</p>");
                                    //    return false;
                                    //}
                                    //else if ((eval($("#txtQty_" + ItmID + "").val()) + eval(userItemQuantityInCart)) > eval(categoryDetails.info.MaxCartQuantity)) {
                                    //    csscody.alert("<h2>" + getLocale(DetailsBrowse, 'Information Alert') + '</h2><p>' + getLocale(DetailsBrowse, 'The requested quantity for this item is not valid') + "</p>");
                                    //    return false;
                                    //}
                                    //else 
                                    if (BrandItemList.info.Quantity <= 0 && Qty <= 0) {
                                        csscody.alert("<h2>" + getLocale(DetailsBrowse, 'Information Alert') + '</h2><p>' + getLocale(DetailsBrowse, 'This product is currently Out Of Stock!') + "</p>");
                                        return false;
                                    } else {
                                        if ((eval($.trim($("#txtQty_" + ItmID + "").val())) + eval(itemQuantityInCart)) > eval(BrandItemList.info.Quantity) && (eval($.trim($("#txtQty_" + ItmID + "").val())) + eval(itemQuantityInCart)) > eval(Qty)) {
                                            csscody.alert("<h2>" + getLocale(DetailsBrowse, 'Information Alert') + '</h2><p>' + getLocale(DetailsBrowse, 'This product is currently Out Of Stock!') + "</p>");
                                            return false;
                                        }
                                    }
                                }
                            }
                            else {
                                $("#btnAddToMyCart_" + ItmID + "").parent('label').remove();
                            }
                        }
                        if (BrandItemList.info.IsPricePercentage) {
                            costVariantPrice = eval($("#hdnPrice_" + ItmID + "").val()) * eval(BrandItemList.info.PriceModifier) / 100;
                        } else {
                            costVariantPrice = eval(BrandItemList.info.PriceModifier);
                        }
                        if (BrandItemList.info.IsWeightPercentage) {
                            weightWithVariant = eval($("#hdnWeight_" + ItmID + "").val()) * eval(BrandItemList.info.WeightModifier) / 100;
                        } else {
                            weightWithVariant = eval(BrandItemList.info.WeightModifier);
                        }

                        totalWeightVariant = eval($("#hdnWeight_" + ItmID + "").val()) + eval(weightWithVariant);
                        itemPrice = eval(itemPrice) + eval(costVariantPrice);
                        var AddItemToCartObj = {
                            ItemID: ItmID,
                            Price: itemPrice,
                            Weight: totalWeightVariant,
                            Quantity: itemQuantity,
                            CostVariantIDs: itemCostVariantIDs.join('@') + '@',
                            IsGiftCard: false,
                            IsKitItem: false
                        };
                        var paramz = {
                            aspxCommonObj: aspxCommonObj,
                            AddItemToCartObj: AddItemToCartObj,
                            giftCardDetail: {},
                            kitInfo: {}
                        };
                        var data = JSON2.stringify(paramz);
                        this.config.method = "AddItemstoCartFromDetail";
                        this.config.url = this.config.baseURL + this.config.method;
                        this.config.data = data;
                        this.config.ajaxCallMode = BrandItemList.AddItemstoCartFromDetail;
                        this.config.oncomplete = 20;
                        this.config.error = BrandItemList.GetAddToCartErrorMsg;
                        this.ajaxCall(this.config);
                    } else {
                        csscody.alert('<h2>' + getLocale(DetailsBrowse, 'Information Alert') + '</h2><p>' + getLocale(DetailsBrowse, 'Please choose available variants!') + '</p>');
                    }
                }

            },
            GetAddToCartErrorMsg: function () {

                csscody.error('<h2>' + getLocale(DetailsBrowse, 'Information Alert') + '</h2><p>' + getLocale(DetailsBrowse, 'Failed to add item to cart!') + '</p>');
            },
            AddItemstoCartFromDetail: function (msg) {
                //debugger;
                if (msg.d == 1) {
                    var myCartUrl;
                    myCartUrl = myCartURL + pageExtension;
                    var addToCartProperties = {
                        onComplete: function (e) {
                            if (e) {
                                window.location.href = SanchiCommerce.utils.GetAspxRedirectPath() + myCartURL + pageExtension;
                            }
                        }
                    };
                    csscody.addToCart('<h2>' + getLocale(DetailsBrowse, "Successful Message") + '</h2><p>' + getLocale(DetailsBrowse, 'Item has been successfully added to cart.') + '</p>', addToCartProperties);
                    if (allowRealTimeNotifications.toLowerCase() == 'true') {
                        try {
                            var itemOnCart = $.connection._aspxrthub;
                            itemOnCart.server.checkIfItemOutOfStock(itemId, itemSKU, "", SanchiCommerce.AspxCommonObj());

                        }
                        catch (Exception) {
                            console.log(getLocale(DetailsBrowse, 'Error Connecting Hub.'));
                        }
                    }
                    HeaderControl.GetCartItemTotalCount(); ShopingBag.GetCartItemCount(); ShopingBag.GetCartItemListDetails();
                }
                else if (msg.d == 2) {
                    if (allowOutStockPurchase.toLowerCase() == 'false') {
                        csscody.alert("<h2>" + getLocale(DetailsBrowse, 'Information Alert') + '</h2><p>' + getLocale(AspxItemDetailsBrowseDetails, 'This product is currently Out Of Stock!') + "</p>");
                    }
                    else {
                        var myCartUrl = myCartURL + pageExtension;
                        var addToCartProperties = {
                            onComplete: function (e) {
                                if (e) {
                                    window.location.href = SanchiCommerce.utils.GetAspxRedirectPath() + myCartURL + pageExtension;
                                }
                            }
                        };
                        csscody.addToCart('<h2>' + getLocale(DetailsBrowse, "Successful Message") + '</h2><p>' + getLocale(DetailsBrowse, 'Item has been successfully added to cart.') + '</p>', addToCartProperties);
                        HeaderControl.GetCartItemTotalCount();
                        ShopingBag.GetCartItemCount();
                        ShopingBag.GetCartItemListDetails();
                    }
                }
            },
           
            CheckItemQuantityInCart: function (ID, itemCostVariantIDs) {
                
                BrandItemList.vars.itemQuantityInCart = 0;
                BrandItemList.vars.userItemQuantityInCart = 0;
                $.ajax({
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    async: false,
                    url: aspxservicePath + 'AspxCoreHandler.ashx/CheckItemQuantityInCart',
                    data: JSON2.stringify({ itemID: ID, aspxCommonObj: aspxCommonObj, itemCostVariantIDs: itemCostVariantIDs }),
                    dataType: "json",
                    success: function (data) {

                        BrandItemList.vars.itemQuantityInCart = data.d.ItemQuantityInCart;
                        BrandItemList.vars.userItemQuantityInCart = data.d.UserItemQuantityInCart;
                    },
                    error: function () { }
                });
                var itemsCartInfo = {
                    ItemQuantityInCart: BrandItemList.vars.itemQuantityInCart,
                    UserItemQuantityInCart: BrandItemList.vars.userItemQuantityInCart
                };
                return itemsCartInfo;
            },
            Init: function () {
                $.each(jsTemplateArray, function(index, value) {
                    var tempVal = jsTemplateArray[index].split('@');
                    var templateScript = {
                        TemplateKey: tempVal[0],
                        TemplateValue: tempVal[1]
                    };
                    templateScriptArr.push(templateScript);
                });
                CreateDdlPageSizeOption('ddlBrandPageSize');
                                BrandItemList.BrandItemsHideAll();
                BrandItemList.GetBrandItemsByBrandID();
                createDropDown('ddlBrandSortBy', 'divSortBy', 'sortBy', displaymode);
                createDropDown('ddlBrandViewAs', 'divBrandViewAs', 'viewAs', displaymode);
                BrandItemList.BindBrandResultItems(1, $('#ddlBrandPageSize').val(), 0, $("#ddlBrandSortBy option:selected").val());


                $("#ddlBrandViewAs").on("change", function() {
                    BindResults('divBrandSearchResult', 'divBrandViewAs', 'ddlBrandViewAs', null,allowAddToCart, allowOutStockPurchase,  noImageBrandItemListByIdsPath, noOfItemsInRow, displaymode);

                });
                $("#divBrandViewAs").find('a').on('click', function() {
                    $("#divBrandViewAs").find('a').removeClass('sfactive');
                    $(this).addClass("sfactive");
                    BindResults('divBrandSearchResult', 'divBrandViewAs', 'ddlBrandViewAs', null,allowAddToCart, allowOutStockPurchase,  noImageBrandItemListByIdsPath, noOfItemsInRow, displaymode);

                });

                $("#ddlBrandSortBy").on("change", function() {
                    var items_per_page = $("#ddlBrandPageSize").val();
                    var offset = 1;
                    BrandItemList.BindBrandResultItems(offset, items_per_page, 0, $("#ddlBrandSortBy option:selected").val());
                });

                $("#ddlBrandPageSize").bind("change", function() {
                    var items_per_page = $(this).val();
                    var offset = 1;
                    BrandItemList.BindBrandResultItems(offset, items_per_page, 0, $("#ddlBrandSortBy option:selected").val());
                });
            }
        };
        BrandItemList.Init();
    });

    //]]>
    function selectFirstcombination(ItemId) {
        //debugger;
        if (arrCombination.length > 0) {

            var Combination = [];
            $.each(arrCombination, function (index, item) {

                if (ItemId == item.ItemID) {

                    Combination.push(item);
                }

            });
            var cvcombinationList = getObjects(arrCombination, 'CombinationType');
            var cvValuecombinationList = getObjects(arrCombination, 'CombinationValues');
            var x = cvcombinationList[0].split('@');
            var y = cvValuecombinationList[0].split('@');
            $("#Notify").hide();
            $("#divCostVariant_" + ItemId + " select").each(function (i) {

                if (parseFloat($(this).parent("span:eq(0)").prop('id').split('_')[0].replace('subDiv', '')) == x[i]) {
                    if ($(this).find("option[value=" + y[i] + "]").length > 0) {
                        $(this).find("option[value=" + y[i] + "]").prop('selected', 'selected');
                    }
                    else {

                        var options = $(this).html();
                        //var noOption = "<option value='0'>" + getLocale(DetailsBrowse, "Not required") + "</option>";
                        $(this).html(options);
                        //$(this).find('option[value=0]').prop('selected', 'selected');
                    }
                }
                else {
                    var val = parseFloat($(this).parent("span:eq(0)").prop('id').replace('subDiv', ''));
                    var xIndex = 0;
                    for (var indx = 0; indx < x.length; indx++) {
                        if (x[indx] == val) {
                            xIndex = indx;
                            break;
                        }
                    }
                    if ($(this).find("option[value=" + y[xIndex] + "]").length > 0) {
                        $(this).find("option[value=" + y[xIndex] + "]").prop('selected', 'selected');

                    } else {

                        var options = $(this).html();
                        var noOption = "<option value='0'>" + getLocale(DetailsBrowse, "Not required") + "</option>";
                        $(this).html(noOption + options);
                        $(this).find('option[value=0]').prop('selected', 'selected');
                    }
                }
            });

            $("#divCostVariant_" + ItemId + " input[type=radio]").each(function (i) {
                if (parseFloat($(this).parents("span:eq(0)").prop('id').replace('subDiv', '')) == x[i]) {
                    if ($(this).val() == y[i]) {
                        $(this).prop('checked', 'checked').addClass("cssRadioChecked");
                    }
                    else {
                        $(this).removeAttr('checked');
                    }
                } else {
                    var val = parseFloat($(this).parents("span:eq(0)").prop('id').replace('subDiv', ''));
                    var xIndex = 0;
                    for (var indx = 0; indx < x.length; indx++) {
                        if (x[indx] == val) {
                            xIndex = indx;
                            break;
                        }
                    }
                    if ($(this).val() == y[xIndex]) {
                        $(this).prop('checked', 'checked').addClass("cssRadioChecked");

                    } else {
                        $(this).removeAttr('checked');
                    }
                }
            });

            $("#divCostVariant_" + ItemId + " input[type=checkbox]:checked").each(function (i) {
                if (parseFloat($(this).parent("span:eq(0)").prop('id').replace('subDiv', '')) == x[i]) {
                    if ($(this).val() == y[i]) {
                        $(this).prop('checked', 'checked');

                    } else {
                        $(this).removeAttr('checked');
                    }
                } else {
                    var val = parseFloat($(this).parent("span:eq(0)").prop('id').replace('subDiv', ''));
                    var xIndex = 0;
                    for (var indx = 0; indx < x.length; indx++) {
                        if (x[indx] == val) {
                            xIndex = indx;
                            break;
                        }
                    }

                    if ($(this).val() == y[xIndex]) {
                        $(this).prop('checked', 'checked');

                    } else {
                        $(this).removeAttr('checked');
                    }
                }
            });


            CheckVariantCombination(cvcombinationList[0], cvValuecombinationList[0], x[0], y[0], ItemId);
            //$("#spanAvailability_" + ItemId + "").html(categoryDetails.info.IsCombinationMatched == true ? '<b>' + getLocale(DetailsBrowse, 'In stock') + '</b>' : '<b>' + getLocale(DetailsBrowse, 'In stock') + '</b>');
            if (BrandItemList.info.IsCombinationMatched == true) {
                $("#btnAddToMyCart_" + ItemId + "").removeClass("cssClassOutOfStock").addClass('addtoCart ').removeAttr("disabled").prop('enabled', "enabled").find("span").html(getLocale(DetailsBrowse, "Cart +"));
                $("#btnAddToMyCart_" + ItemId + "").parent('label').addClass('i-cart cssClassCartLabel cssClassGreenBtn');
            }
            else {
                $("#btnAddToMyCart_" + ItemId + "").removeClass("cssClassOutOfStock").addClass('addtoCart ').removeAttr("disabled").prop('enabled', "enabled").find("span").html(getLocale(DetailsBrowse, "Cart +"));
                $("#btnAddToMyCart_" + ItemId + "").parent('label').addClass('i-cart cssClassCartLabel cssClassGreenBtn');
                //$("#btnAddToMyCart_" + ItemId + "").removeClass("addtoCart").addClass('cssClassOutOfStock').prop("disabled", "disabled").find("span").html(getLocale(DetailsBrowse, "Out Of Stock"));
                //$("#btnAddToMyCart_" + ItemId + "").parent('label').removeClass('i-cart cssClassCartLabel cssClassGreenBtn');
            }
            if (BrandItemList.info.IsCombinationMatched) {

                $("#hdnQuantity_" + ItemId + "").val('').val(BrandItemList.info.Quantity);
                $("#txtQty_" + ItemId + "").removeAttr('disabled').prop("enabled", "enabled");
                if (BrandItemList.info.Quantity == 0 || BrandItemList.info.Quantity < 0) {
                    if (allowAddToCart.toLowerCase() == 'true') {
                        if (allowOutStockPurchase.toLowerCase() == 'false') {

                            $(".cssClassAddtoCard_" + ItemId + " .sfButtonwrapper").addClass('cssClassOutOfStockcolor');
                            $("#btnAddToMyCart_" + ItemId + "").removeClass("addtoCart ").addClass('cssClassOutOfStock').prop("disabled", "disabled").find("span").html(getLocale(DetailsBrowse, "Out Of Stock"));
                            $("#btnAddToMyCart_" + ItemId + "").parent('label').removeClass('i-cart cssClassCartLabel cssClassGreenBtn');
                            $("#spanAvailability_" + ItemId + "").html('<b>' + getLocale(DetailsBrowse, 'Out Of Stock') + '</b>');
                            $("#spanAvailability_" + ItemId + "").addClass('cssOutOfStock');
                            $("#spanAvailability_" + ItemId + "").removeClass('cssInStock');
                            //if (userName != "anonymoususer") {
                            //    $("#Notify").show();
                            //    $("#Notify #txtNotifiy").hide();
                            //} else {
                            //    $("#Notify").show();
                            //    $("#txtNotifiy").show();
                            //}
                        }
                    }
                    else {
                        $("#btnAddToMyCart_" + ItemId + "").parent('label').remove();
                    }

                } else {
                    $("#btnAddToMyCart_" + ItemId + "").removeClass("cssClassOutOfStock").addClass('addtoCart ').removeAttr("disabled").prop('enabled', "enabled").find("span").html(getLocale(DetailsBrowse, "Cart +"));
                    $("#btnAddToMyCart_" + ItemId + "").parent('label').addClass('i-cart cssClassCartLabel cssClassGreenBtn');
                    $("#spanAvailability_" + ItemId + "").html('<b>' + getLocale(DetailsBrowse, 'In stock') + '</b>');
                    $("#spanAvailability_" + ItemId + "").removeClass('cssOutOfStock');
                    $("#spanAvailability_" + ItemId + "").addClass('cssInStock');
                    $("#Notify").hide();
                }
                var values = getSelectecdVariantValues().Values;

                var itemsCartInfo = BrandItemList.CheckItemQuantityInCart(ItemId, values.join('@') + '@');
                var quantityinCart = itemsCartInfo.ItemQuantityInCart;
                if (BrandItemList.info.Quantity <= quantityinCart) {
                    if (allowAddToCart.toLowerCase() == 'true') {
                        if (allowOutStockPurchase.toLowerCase() == 'false') {

                            $(".cssClassAddtoCard_" + ItemId + " .sfButtonwrapper").addClass('cssClassOutOfStockcolor');

                            $("#txtQty_" + ItemId + "").removeAttr('enabled').prop("disabled", "disabled");
                            $("#btnAddToMyCart_" + ItemId + "").removeClass("addtoCart ").addClass('cssClassOutOfStock').prop("disabled", "disabled").find("span").html(getLocale(DetailsBrowse, "Out Of Stock"));
                            $("#btnAddToMyCart_" + ItemId + "").parent('label').removeClass('i-cart cssClassCartLabel cssClassGreenBtn');
                            $("#spanAvailability_" + ItemId + "").html('<b>' + getLocale(DetailsBrowse, 'Out Of Stock') + '</b>');
                            $("#spanAvailability_" + ItemId + "").addClass('cssOutOfStock');
                            $("#spanAvailability_" + ItemId + "").removeClass('cssInStock');
                            //if (userName != "anonymoususer") {
                            //    $("#Notify").show();
                            //    $("#Notify #txtNotifiy").hide();
                            //} else {
                            //    $("#Notify").show()
                            //    $("#txtNotifiy").show();
                            //}
                        }
                    }
                    else {
                        $("#btnAddToMyCart_" + ItemId + "").parent('label').remove();
                    }
                }
                var price = 0;
                if (BrandItemList.info.IsPricePercentage) {
                    price = eval($("#hdnPrice_" + ItemId + "").val()) * eval(BrandItemList.info.PriceModifier) / 100;
                } else {
                    price = eval(BrandItemList.info.PriceModifier);
                }

                $("#spanPrice_" + ItemId + "").html(parseFloat((eval($("#hdnPrice_" + ItemId + "").val())) + (eval(price))).toFixed(2));
                $("#spanPrice_" + ItemId + "").attr('bc', parseFloat((eval($("#hdnPrice_" + ItemId + "").val())) + (eval(price))).toFixed(2));
                //$("#spanPrice_" + ItemId + "").html(parseFloat((eval(price))).toFixed(2));
                //$("#spanPrice_" + ItemId + "").attr('bc', parseFloat((eval(price))).toFixed(2));
                var taxPriceVariant = eval($("#hdnPrice_" + ItemId + "").val()) + eval(price);
                var taxrate = (parseFloat(($("#hdnTaxRateValue").val()) * 100) / (eval($("#hdnPrice_" + ItemId + "").val())).toFixed(2));
                if ($("#hdnListPrice").val() != '') {
                    $(".cssClassYouSave").show();
                    var variantAddedPrice = eval($("#hdnPrice").val()) + eval(price);
                    var variantAddedSavingPercent = (($("#hdnListPrice").val() - variantAddedPrice) / $("#hdnListPrice").val()) * 100;
                    savingPercent2 = variantAddedSavingPercent.toFixed(2);
                    $("#spanSaving").html('<b>' + variantAddedSavingPercent.toFixed(2) + '%</b>');
                }
            }
        }
        var cookieCurrency = $("#ddlCurrency").val();
        $("#spanPrice_" + ItemId + "").removeAttr('data-currency-' + cookieCurrency + '');
        $("#spanPrice_" + ItemId + "").removeAttr('data-currency');
        Currency.currentCurrency = BaseCurrency;
        Currency.convertAll(Currency.currentCurrency, cookieCurrency);
    }
    function getModifiersByObjects(obj, combinationId) {
        var modifiers = { Price: '', IsPricePercentage: false, Weight: '', IsWeightPercentage: false, Quantity: 0 };

        for (var i in obj) {
            if (!obj.hasOwnProperty(i)) continue;
            if (typeof obj[i] == 'object') {
                if (obj[i]["CombinationID"] == combinationId) {
                    modifiers.Price = obj[i]["CombinationPriceModifier"];
                    modifiers.IsPricePercentage = obj[i]["CombinationPriceModifierType"];
                    modifiers.Weight = obj[i]["CombinationWeightModifier"];
                    modifiers.IsWeightPercentage = obj[i]["CombinationWeightModifierType"];
                    modifiers.Quantity = obj[i]["CombinationQuantity"];
                }
            }

        }
        return modifiers;
    }
    function checkAvailibility(elem) {
        if (elem.length != 0) {
            var cvids = [];
            var values = [];
            var currentValue = elem == null ? 1 : $(elem).val();
            var currentCostVariant = elem == null ? 1 : $(elem).parents('span:eq(0)').prop('id').split('_')[0].replace('subDiv', '');
            ItemId = $(elem).parents('span:eq(0)').prop('id').split('_')[1];
            $("#Notify").hide();
            $("#divCostVariant_" + ItemId + " select option:selected").each(function () {
                if (this.value != 0) {
                    values.push(this.value);
                    cvids.push($(this).parents("span:eq(0)").prop('id').split('_')[0].replace('subDiv', ''));
                }
            });

            $("#divCostVariant_" + ItemId + " input[type=radio]:checked").each(function () {
                if ($(this).is(":checked"))
                { $(this).addClass("cssRadioChecked") }
                else { $(this).removeClass("cssRadioChecked"); }
                values.push(this.value);
                cvids.push($(this).parents("span:eq(0)").prop('id').split('_')[0].replace('subDiv', ''));
            });

            $("#divCostVariant_" + ItemId + " input[type=radio]").each(function () {
                if ($(this).is(":checked"))
                { $(this).addClass("cssRadioChecked") }
                else { $(this).removeClass("cssRadioChecked"); }
            });

            $("#divCostVariant_" + ItemId + " input[type=checkbox]:checked").each(function () {
                values.push(this.value);
                cvids.push($(this).parents("span:eq(0)").prop('id').split('_')[0].replace('subDiv', ''));
            });

            var infos = CheckVariantCombination(cvids.join('@'), values.join('@'), currentCostVariant, currentValue, ItemId);


            //$("#spanAvailability_" + ItemId + "").html(categoryDetails.info.IsCombinationMatched == true ? '<b>' + getLocale(DetailsBrowse, 'In stock') + '</b>' : '<b>' + getLocale(DetailsBrowse, 'Not available') + '</b>');
            if (BrandItemList.info.IsCombinationMatched == true) {
                $("#btnAddToMyCart_" + ItemId + "").removeClass("cssClassOutOfStock").addClass('addtoCart ').removeAttr("disabled").prop('enabled', "enabled").find("span").html(getLocale(DetailsBrowse, "Cart +"));
                $("#btnAddToMyCart_" + ItemId + "").parent('label').addClass('i-cart cssClassCartLabel cssClassGreenBtn');
            }
            else {
                $("#btnAddToMyCart_" + ItemId + "").removeClass("cssClassOutOfStock").addClass('addtoCart ').removeAttr("disabled").prop('enabled', "enabled").find("span").html(getLocale(DetailsBrowse, "Cart +"));
                $("#btnAddToMyCart_" + ItemId + "").parent('label').addClass('i-cart cssClassCartLabel cssClassGreenBtn');

                //$("#btnAddToMyCart_" + ItemId + "").removeClass("addtoCart").addClass('cssClassOutOfStock').prop("disabled", "disabled").find("span").html(getLocale(DetailsBrowse, "Out Of Stock"));
                //$("#btnAddToMyCart_" + ItemId + "").parent('label').removeClass('i-cart cssClassCartLabel cssClassGreenBtn');
            }

            if (BrandItemList.info.IsCombinationMatched) {
                $("#hdnQuantity_" + ItemId + "").val('').val(BrandItemList.info.Quantity);
                $("#txtQty_" + ItemId + "").removeAttr('disabled').prop("enabled", "enabled");
                if (BrandItemList.info.Quantity == 0 || BrandItemList.info.Quantity < 0) {
                    if (allowAddToCart.toLowerCase() == 'true') {
                        if (allowOutStockPurchase.toLowerCase() == 'false') {
                            $("#spanAvailability_" + ItemId + "").html("<b>" + getLocale(DetailsBrowse, "Out Of Stock") + "</b>");
                            $("#spanAvailability_" + ItemId + "").addClass('cssOutOfStock');
                            $("#spanAvailability_" + ItemId + "").removeClass('cssInStock');
                            //if (userName != "anonymoususer") {
                            //    $("#Notify").show();
                            //    $("#Notify #txtNotifiy").hide();
                            //}
                            //else {
                            //    $("#Notify").show();
                            //    $("#txtNotifiy").show();
                            //}
                        }
                    }
                    else {
                        $("#btnAddToMyCart_" + ItemId + "").parent('label').remove();
                    }

                } else {
                    $("#spanAvailability_" + ItemId + "").html('<b>' + getLocale(DetailsBrowse, 'In stock') + '</b>' + '</b>');
                    $("#spanAvailability_" + ItemId + "").removeClass('cssOutOfStock');
                    $("#spanAvailability_" + ItemId + "").addClass('cssInStock');
                    $("#Notify").hide();
                }
                var itemsCartInfo = BrandItemList.CheckItemQuantityInCart(ItemId, values.join('@') + '@');
                var quantityinCart = itemsCartInfo.ItemQuantityInCart;
                if (BrandItemList.info.Quantity <= quantityinCart) {
                    if (allowAddToCart.toLowerCase() == 'true') {
                        if (allowOutStockPurchase.toLowerCase() == 'false') {

                            $(".cssClassAddtoCard_" + ItemId + " .sfButtonwrapper").addClass('cssClassOutOfStockcolor');

                            $("#txtQty_" + ItemId + "").removeAttr('enabled').prop("disabled", "disabled");
                            $("#btnAddToMyCart_" + ItemId + "").removeClass("addtoCart ").addClass('cssClassOutOfStock').prop("disabled", "disabled").find("span").html(getLocale(DetailsBrowse, "Out Of Stock"));
                            $("#btnAddToMyCart_" + ItemId + "").parent('label').removeClass('i-cart cssClassCartLabel cssClassGreenBtn');
                            $("#spanAvailability_" + ItemId + "").html("<b>" + getLocale(DetailsBrowse, "Out Of Stock") + "</b>");
                            $("#spanAvailability_" + ItemId + "").addClass('cssOutOfStock');
                            $("#spanAvailability_" + ItemId + "").removeClass('cssInStock');
                            //if (userName != "anonymoususer") {
                            //    $("#Notify").show();
                            //    $("#Notify #txtNotifiy").hide();
                            //}
                            //else {
                            //    $("#Notify").show();
                            //    $("#txtNotifiy").show();
                            //}
                        }
                    }
                    else {
                        $("#btnAddToMyCart_" + ItemId + "").parent('label').remove();
                    }
                }
                var price = 0;
                if (BrandItemList.info.IsPricePercentage) {
                    price = eval($("#hdnPrice_" + ItemId + "").val()) * eval(BrandItemList.info.PriceModifier) / 100;
                } else {
                    price = eval(BrandItemList.info.PriceModifier);
                }

                $("#spanPrice_" + ItemId + "").html(parseFloat((eval($("#hdnPrice_" + ItemId + "").val())) + (eval(price))).toFixed(2));
                $("#spanPrice_" + ItemId + "").attr('bc', parseFloat((eval($("#hdnPrice_" + ItemId + "").val())) + (eval(price))).toFixed(2));
                //$("#spanPrice_" + ItemId + "").html(parseFloat((eval(price))).toFixed(2));
                //$("#spanPrice_" + ItemId + "").attr('bc', parseFloat((eval(price))).toFixed(2));

                var taxPriceVariant = eval($("#hdnPrice_" + ItemId + "").val()) + eval(price);
                var taxrate = (eval($("#hdnTaxRateValue").val()) * 100) / (eval($("#hdnPrice_" + ItemId + "").val()));
                $("#spanTax").html(parseFloat(((taxPriceVariant * taxrate) / 100)).toFixed(2));
                if ($("#hdnListPrice_" + ItemId + "").val() != '') {
                    $(".cssClassYouSave").show();
                    var variantAddedPrice = eval($("#hdnPrice_" + ItemId + "").val()) + eval(price);
                    var variantAddedSavingPercent = (($("#hdnListPrice_" + ItemId + "").val() - variantAddedPrice) / $("#hdnListPrice_" + ItemId + "").val()) * 100;
                    savingPercent2 = variantAddedSavingPercent.toFixed(2);
                    $("#spanSaving").html('<b>' + variantAddedSavingPercent.toFixed(2) + '%</b>');
                }
                //categoryDetails.ResetGallery(categoryDetails.info.CombinationID);

            } else {
                $("#btnAddToMyCart_" + ItemId + "").removeClass("cssClassOutOfStock").addClass('addtoCart ').removeAttr("disabled").prop('enabled', "enabled").find("span").html(getLocale(DetailsBrowse, "Cart +"));
                $("#btnAddToMyCart_" + ItemId + "").parent('label').addClass('i-cart cssClassCartLabel cssClassGreenBtn');
                $("#spanAvailability_" + ItemId + "").removeClass('cssOutOfStock');
                $("#spanAvailability_" + ItemId + "").removeClass('cssInStock');

            }
            var cookieCurrency = $("#ddlCurrency").val();
            $("#spanPrice_" + ItemId + "").removeAttr('data-currency-' + cookieCurrency + '');
            $("#spanPrice_" + ItemId + "").removeAttr('data-currency');
            Currency.currentCurrency = BaseCurrency;
            Currency.convertAll(Currency.currentCurrency, cookieCurrency);
        }
        else { BrandItemList.info.IsCombinationMatched = true; }
    }
    function getSelectecdVariantValues() {

        var cvids = [];
        var values = [];

        $("#divCostVariant_" + ItemId + " select option:selected").each(function () {
            if (this.value != 0) {
                values.push(this.value);
                cvids.push($(this).parents("span:eq(0)").prop('id').replace('subDiv', ''));
            }
        });

        $("#divCostVariant_" + ItemId + " input[type=radio]:checked").each(function () {
            if ($(this).is(":checked"))
            { $(this).addClass("cssRadioChecked") }
            else { $(this).removeClass("cssRadioChecked"); }
            values.push(this.value);
            cvids.push($(this).parents("span:eq(0)").prop('id').replace('subDiv', ''));
        });

        $("#divCostVariant_" + ItemId + " input[type=radio]").each(function () {
            if ($(this).is(":checked"))
            { $(this).addClass("cssRadioChecked") }
            else { $(this).removeClass("cssRadioChecked"); }
        });

        $("#divCostVariant_" + ItemId + " input[type=checkbox]:checked").each(function () {
            values.push(this.value);
            cvids.push($(this).parents("span:eq(0)").prop('id').replace('subDiv', ''));
        });
        return { Values: values, CVIds: cvids };
    
    }
</script>

<h2 class='sfBrandHeaderTitle cssClassBMar20'>
</h2>
<div class="cssClassBrandSlider" id="divBrandHeader">
</div>
<div id="divBrandItemViewOptions" class="viewWrapper" style="display:none">
    <div id="divBrandViewAs" class="view">
        <span class="sfLocale">View as:</span>
        <select id="ddlBrandViewAs" class="sfListmenu" style="display: none">
        </select>
    </div>
    <div id="divSortBy" class="sort">
        <span class="sfLocale">Sort by:</span>
        <select id="ddlBrandSortBy" class="sfListmenu">
        </select>
    </div>
</div>
<div id="divBrandSearchResult" class="cssClassDisplayResult">
</div>
<div class="cssClassClear">
</div>
<!-- TODO:: paging Here -->
<div class="cssClassPageNumber" id="divBrandPageNumber" style="display:none">
    <div class="cssClassPageNumberMidBg clearfix">
        <div id="BrandPagination">
        </div>
        <div class="cssClassViewPerPage">
            <span class="sfLocale">
                View Per Page:
            </span>
            <select id="ddlBrandPageSize" class="sfListmenu">
                <option value=""></option>
            </select></div>
    </div>
</div>
