﻿using SageFrame.Web;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SageFrame.Web.Utilities;

public partial class Modules_SanchiCommerce_AspxOrderTracking_TrackYourOrder : BaseAdministrationUserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        IncludeJs("AspxOrderTracking", "/Modules/SanchiCommerce/AspxOrderTracking/js/TrackYourOrder.js");
        IncludeCss("AspxOrderTracking", "/Modules/SanchiCommerce/AspxOrderTracking/css/order-track.css");
        
    }
}