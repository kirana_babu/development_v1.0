﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="SageMenu.ascx.cs" Inherits="Modules_SageMenu_SageMenu" %>
<%-- <div id="textCategories">
        <p>Categories<span class="i-menu"></span></p>
    </div>--%>
<asp:Literal ID="ltrNav" runat="server" EnableViewState="false"></asp:Literal>
<script type="text/javascript">
    var toggle = 0;
    var timeout;
    $(function () {
        $(this).SageMenuBuilder({
            ContainerClientID: '<%=ContainerClientID %>',
            MenuType: '<%=menuType%>'
        });
        ResponsiveCategory(); $(window).resize(function () { ResponsiveCategory(); }).resize();
    });
    function ResponsiveCategory() {
        var windowsWidth2 = $(window).width();
        if (windowsWidth2 > 960) {
            $(".sf-menu").show();
            $(".sfLogininfo").show();
            $("#sf-Responsive-Menu").hide();
            //$(".sfModuleLogo").removeAttr('style');
            //$(".sfModuleLogo").next().removeAttr('style').css("display", "none");
            //$("#searchlens").show();
            //$("#searchbutt").hide();
            //$("#divmiddlebanner").show();
        }
        else {
            setMobileNav(); clearTimeout(timeout); timeout = setTimeout(function () { toggle = 0; $(".sf-MenuContainer").slideUp(); setMobileNav(); }, 300);
        }
    }
    function setMobileNav() {
        //$(".sfModuleLogo").next().removeAttr('style');
        $(".sf-menu").hide();
        $("#sf-Responsive").show().css("width", "100%");
        $("#sf-Responsive-Menu").show().css("width", "100%");
        //$(".sfModuleLogo").css("float", "left").css("width", "91%");
        //$(".sfModuleLogo").next().css("display", "block!important").css("float", "left").css("width", "9%").css("padding-top", "15px");
        $("#sf-Responsive-Menu").html("").append($(".sf-menu").clone());
        $("#sf-Responsive-Menu ul").removeAttr('class').removeAttr('style').css("display", "none");
        //$("#sf-Responsive-Menu li").removeAttr('class').removeAttr('style').css("width", "100%");
        $("#sf-Responsive-Menu li:has(> ul)").addClass('parent');
        $($("#sf-Responsive-Menu ul")[0]).addClass("sf-MenuContainer");
        $("#sf-Menu").unbind("click");
        $("#sf-Menu").on("click", function () { $(".sf-MenuContainer").slideToggle(); });
        $("<strong class=\"catViewer\"><i class=\"i-plus\"></i></strong>").insertAfter($('.sf-MenuContainer').find('li.parent>a'));
        $('.sf-MenuContainer .catViewer>i').unbind("click");
        $('.sf-MenuContainer .catViewer>i').on("click", function () {
            var $parentLi = $(this).parents('li.parent:eq(0)');
            var goDown = $(this).hasClass('i-plus');
            if (goDown) { $parentLi.find('ul').hide(); }
            else {
                $parentLi.removeClass('cssOnclick');
                $parentLi.find('ul:gt(1)').hide(); $parentLi.find('ul:first').slideToggle(); if (goDown) { $(this).removeClass('i-plus').addClass('i-minus'); } else { $(this).removeClass('i-minus').addClass('i-plus'); } $parentLi.find('li.parent .catViewer>i').removeClass('i-minus').addClass('i-plus'); return;
            }
            if ($parentLi.hasClass('slidedown')) {
                $parentLi.find('ul:first').slideToggle(); $parentLi.addClass('cssOnclick');
            }
            else {
                $parentLi.addClass('slidedown').addClass('cssOnclick').find('ul:first').slideToggle();
            }
            if (goDown) {
                $(this).removeClass('i-plus').addClass('i-minus');
            } else { $(this).removeClass('i-minus').addClass('i-plus'); }
        });
    }
</script>

