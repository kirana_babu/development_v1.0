﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="NewsLetterView.ascx.cs"
    Inherits="Modules_NewsLetter_NewsLetterView" %>
<script type="text/javascript">
    $(function() {
        $(".sfLocalee").SystemLocalize();
    });
    var UserModuleID = '<%=UserModuleID %>';
    var PortalID = '<%=PortalID %>';
    var NewsLetterPath = '<%=ModulePath %>';
    var UserName = '<%=UserName %>';
    var PageExt = '<%=PageExt %>';        
</script>
<div id="divSubscribe" class="sfSubscribe">
    <div id="divRadios" style="display:none">
        <input type="radio" name="rdbSubcribe" class="sfLocalee" value="By Email" checked="checked"/>
    </div>
    <div id="divEmailSubsCribe">
        <div id="divEmailtext"><p>Newsletter</p></div>
          <div style="margin:10px 0">
        <input name="Email" type="text" id="txtSubscribeEmail" class="sfInputbox cssClassBMar10" placeholder="Enter Your E-mail" />
        <input id="btnSubscribe" type="button" class="sfBtn sfLocalee cssClassBlueBtn" value="Ok" />
        <label id="lblmessage" style="font-weight: 100;text-align: center;padding-left: 5px;display:none" class="sfMessage sfLocale">text</label>
              </div>
    </div>
</div>
