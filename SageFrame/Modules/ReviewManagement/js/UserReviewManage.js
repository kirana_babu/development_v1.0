﻿var UserRaviewManage = "";

$(function () {
    var UserRatingMgmtFlag = 0;
    var userName = SanchiCommerce.utils.GetUserName();
    var userIP = SanchiCommerce.utils.GetClientIP();
    var isReloadFlag = 0;
    var aspxCommonObj = {
        StoreID: SanchiCommerce.utils.GetStoreID(),
        PortalID: SanchiCommerce.utils.GetPortalID(),
        UserName: SanchiCommerce.utils.GetCultureName(),
        CultureName: SanchiCommerce.utils.GetCultureName()
    };
    UserRaviewManage = {
        config: {
            isPostBack: false,
            async: false,
            cache: true,
            type: "POST",
            contentType: "application/json; charset=utf-8",
            data: '{}',
            dataType: "json",
            baseURL: SanchiCommerce.utils.GetAspxServicePath() + "AspxCommonHandler.ashx/",
            url: "",
            method: "",
            oncomplete: 0,
            ajaxCallMode: "",
            error: ""
        },
        init: function () {
            UserRaviewManage.BindAllReviewsAndRatingsGrid(null, null);
            UserRaviewManage.ShowGridTable();
            UserRaviewManage.GetStatusList();
            UserRaviewManage.GetAllUserList();
            UserRaviewManage.vars.editVal = false;
           
            $("#btnUserReviewBack").click(function () {
                UserRaviewManage.ShowGridTable();
            });
            var v = $("#form1").validate({
                rules: {
                    name: {
                        minlength: 2,
                        maxlength: 20
                    },
                    
                    review: "required"
                },
                messages: {
                    name: {
                        required: "*",
                        minlength: "(at least 2 chars)",
                        maxlength: "(at most 20 chars)"
                    },
                    
                    review: "*"
                }
            });
            $("#btnSubmitUserReview").click(function () {
                    if (v.form()) {
                        UserRaviewManage.SaveUserRatings();
                        return false;
                    } else {
                        return false;
                    }
                
            });

        },
        vars: {
            ratingValues: '',
            editVal: false
        },
        SaveUserRatings: function () {
            var statusId = $("#selectUserStatus").val();
            var ratingValue = UserRaviewManage.vars.ratingValues;
            var nickName = $("#NickName").val();
            var UserReviewId = $("#hdnUserReview").val();
            UserRatingMgmtFlag = UserReviewId;
            var review = Encoder.htmlEncode($("#txtUserReview").val());
            var param = '';
            var ratingMangObj = {
                UserName: nickName,
                Review: review,
                Rating: ratingValue,
                StatusID: statusId,
                UserReviewId:UserReviewId
            };
            param = JSON2.stringify({ ratingMangObj: ratingMangObj, aspxCommonObj: aspxCommonObj });
            this.config.url = this.config.baseURL + "UpdateUserRating";
            this.config.data = param;
            this.config.ajaxCallMode = 3;
            this.ajaxCall(this.config);
        },
        HideAll: function () {
            $("#divShowUserRatingDetails").hide();
            $("#divUserRatingForm").hide();
        },
        GetAllUserList: function () {
            this.config.url = this.config.baseURL + "GetAllUserList";
            this.config.data = JSON2.stringify({ searchText: "", aspxCommonObj: aspxCommonObj });
            this.config.ajaxCallMode = 4;
            this.ajaxCall(this.config);
        },
        GetStatusList: function () {
            this.config.url = this.config.baseURL + "GetStatus";
            this.config.data = JSON2.stringify({ aspxCommonObj: aspxCommonObj });
            this.config.ajaxCallMode = 2;
            this.ajaxCall(this.config);
        },
        ShowGridTable: function () {
            UserRaviewManage.HideAll();
            $("#divShowUserRatingDetails").show();
        },
        ajaxCall: function (config) {
            $.ajax({
                type: UserRaviewManage.config.type,
                contentType: UserRaviewManage.config.contentType,
                cache: UserRaviewManage.config.cache,
                async: UserRaviewManage.config.async,
                data: UserRaviewManage.config.data,
                dataType: UserRaviewManage.config.dataType,
                url: UserRaviewManage.config.url,
                success: UserRaviewManage.ajaxSuccess,
                error: function (err)
                {
                    console.log(err);
                },
                
            });
        },
        BindAllReviewsAndRatingsGrid: function (searchUserName, StatusID) {
            var usersRatingObj = {
                UserName: searchUserName,
                StatusID: StatusID
            };
            this.config.method = "GetUserRating";
            this.config.data = { usersRatingObj: usersRatingObj, aspxCommonObj: aspxCommonObj };
         var data = this.config.data;
         var offset_ = 1;
         var current_ = 1;                                                
         var perpage = ($("#MyReview_pagesize").length > 0) ? $("#MyReview_pagesize :selected").text() : 10;
         $("#MyReview").sagegrid({
             url: this.config.baseURL,
             functionMethod: this.config.method,
             colModel: [
                  { display: getLocale(AspxReviewManagement, 'UserReviewID'), name: 'UserReviewID', cssclass: '', controlclass: '', coltype: 'label', align: 'left', hide: true },
                  { display: getLocale(AspxReviewManagement, 'Rating'), name: 'Rating', cssclass: '', controlclass: '', coltype: 'label', align: 'left',hide:true },
                   { display: getLocale(AspxReviewManagement, 'Review'), name: 'review', cssclass: '', controlclass: '', coltype: '', align: 'left', hide: true },
                    { display: getLocale(AspxReviewManagement, 'Status ID'), name: 'status_id', cssclass: 'cssClassHeadNumber', controlclass: '', coltype: 'label', align: 'left', hide: false },
                    { display: getLocale(AspxReviewManagement, 'Nick Name'), name: 'user_name', cssclass: 'cssClassHeadNumber', controlclass: '', coltype: 'label', align: 'left', hide: false },
                    { display: getLocale(AspxReviewManagement, 'Actions'), name: 'action', cssclass: 'cssClassAction', coltype: 'label', align: 'center' }
             ],
             buttons: [
                 { display: getLocale(AspxReviewManagement, 'Edit'), name: 'edit', enable: true, _event: 'click', trigger: '1', callMethod: 'UserRaviewManage.EditUserReviewsAndRatings', arguments: '1,2,3,4,5,6' },
                 //{ display: getLocale(AspxReviewManagement, 'Delete'), name: 'delete', enable: true, _event: 'click', trigger: '2', arguments: '' }
             ],
             rp: perpage,
             nomsg: getLocale(AspxReviewManagement, "No Records Found!"),
             param: data,
             current: current_,
             pnew: offset_,
             sortcol: { 0: { sorter: false }, 1: { sorter: false }, 13: { sorter: false } }
         });
        },
        BindStarRatingAverage: function (itemAvgRating) {
            $("#divRating").html('');
            var ratingStars = '';
            var ratingTitle = ["Poor", "OK", "Fair", "Good", "Nice"]; var ratingText = [ "1", "2", "3",  "4",  "5"];
            var i = 0;
            ratingStars += '<div class="cssClassToolTip">';
            for (i = 0; i < 5; i++) {
                if (itemAvgRating == ratingText[i]) {
                    ratingStars += '<input name="avgItemRating" type="radio" class="auto-star-avg {split:1}" disabled="disabled" checked="checked" value="' + ratingTitle[i] + '" />';
                    $(".cssClassRatingTitle").html(ratingTitle[i]);
                } else {
                    ratingStars += '<input name="avgItemRating" type="radio" class="auto-star-avg {split:1}" disabled="disabled" value="' + ratingTitle[i] + '" />';
                }
            }
            UserRaviewManage.vars.ratingValues = itemAvgRating;
            ratingStars += '</div>';
            $("#divRating").append(ratingStars);
            var UserAvgRating = '';
            UserAvgRating = JSON2.stringify(itemAvgRating);
            $('input.item-rating-crieteria').rating('select', UserAvgRating);
            $.metadata.setType("class");
            $('input.auto-star-avg').rating();
        },
        EditUserReviewsAndRatings: function (tblID, argus) {
            switch (tblID) {
                case "MyReview":
                    UserRaviewManage.vars.editVal = true;
                 //   UserRaviewManage.ClearReviewForm();
                    UserRaviewManage.BindItemReviewDetails(argus);
                    UserRaviewManage.BindStarRatingAverage(argus[0]);
                    UserRaviewManage.HideAll();
                    $("#divUserRatingForm").show();
                    $("#hdnUserReview").val(argus[3]);
                    $("#trUserList").hide();
                    break;
                default:
                    break;
            }
        },
        BindItemReviewDetails: function (argus) {
            $("#txtUserReview").val(argus[4]);
            $("#selectUserStatus").val(argus[5]);
            $("#" + lblReviewsFromHeading).html(getLocale(AspxReviewManagement, "Edit item's Rating & Review"));
            $("#NickName").val(argus[6]);
        },
        ClearReviewForm: function () {
            $('.auto-submit-star').rating('drain');
            $('.auto-submit-star').removeAttr('checked');
            $('.auto-submit-star').rating('select', -1);
            $("#NickName").val('');
            $("#txtUserReview").val('');
            $("label.error").hide();
            $('#selectStatus').val('2');
            $('#NickName').removeClass('error');
            $('#NickName').parents('td').find('label').remove();
            $('#txtUserReview').removeClass('error');
            $('#txtUserReview').parents('td').find('label').remove();
         
           
        },
        ajaxSuccess: function(data) {
            switch (UserRaviewManage.config.ajaxCallMode) {
            case 0:
                break;
            case 1:
                $.each(data.d, function(index, user) {
                    $("#selectUserName").append("<option value=" + user.UserId + ">" + user.UserName + "</option>");
                });
                break;
                case 2:
                   
                $.each(data.d, function(index, user) {
                    $("#selectUserStatus").append("<option value=" + user.StatusID + ">" + user.Status + "</option>");
                    $("#ddlStatus").append("<option value=" + user.StatusID + ">" + user.Status + "</option>");
                });
                $('#selectUserStatus').val('2');
                break;
          
                case 3: 
                if (UserRatingMgmtFlag > 0) {
                    csscody.info('<h2>' + getLocale(AspxReviewManagement, 'Successful Message') + '</h2><p>' + getLocale(AspxReviewManagement, 'User review and rating has been updated successfully.') + '</p>');
                } else {
                    csscody.info('<h2>' + getLocale(AspxReviewManagement, 'Successful Message') + '</h2><p>' + getLocale(AspxReviewManagement, 'Your review has been saved successfully.') + '</p>');
                }
                UserRaviewManage.ShowGridTable();
                break;
                case 4:
                if (isReloadFlag == 0) {
                    var elem = '';
                    $("#selectItemList").next("select:hidden").html('');
                    elem += '<option value="0">--</option>';
                    $.each(data.d, function(index, value) {
                        elem += '<option  value="' + value.UserID + '">' + value.UserName + '</option>';
                    });
                    $("#selectItemList").html(elem);
                    $("#selectItemList").searchable({
                        methodUrl: UserRaviewManage.config.baseURL + "GetAllUserList",
                        aspxCommonObj: aspxCommonObj
                    });
                }
                else if (isReloadFlag == 1) {
                    var elem = '';
                    $("#selectItemList").siblings("select.jsearchDropSelect").html('');
                    elem += '<option value="0">--</option>';
                    var size = 0;
                    $.each(data.d, function(index, value) {
                        size++;
                        elem += '<option  value="' + value.UserID + '">' + value.UserName + '</option>';
                    });
                    $("#selectItemList").html(elem);
                    $("#selectItemList").siblings("select.jsearchDropSelect").attr('size', size).html(elem);
                }
                $("#lnkItemName").prop("name", $("#selectItemList").val());
                break;
        }
    }
    }
    UserRaviewManage.init();
});