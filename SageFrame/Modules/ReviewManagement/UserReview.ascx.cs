﻿using SageFrame.Web;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Modules_ReviewManagement_UserReview : BaseAdministrationUserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {

        IncludeCss("UserReview",
            "/Templates/" + TemplateName + "/css/GridView/tablesort.css",
            "/Templates/" + TemplateName + "/css/MessageBox/style.css",
            "/Templates/" + TemplateName + "/css/StarRating/jquery.rating.css");

        IncludeJs("UserReview",
            "/js/jquery.searchabledropdown-1.0.8.src.js",
            "/js/FormValidation/jquery.validate.js",
            "/js/GridView/jquery.grid.js",
            "/js/GridView/SagePaging.js",
            "/js/GridView/jquery.global.js",
            "/js/GridView/jquery.dateFormat.js",
            "/js/MessageBox/jquery.easing.1.3.js",
            "/js/MessageBox/alertbox.js",
            "/js/StarRating/jquery.rating.js",
            "/js/StarRating/jquery.rating.pack.js",
            "/Modules/ReviewManagement/js/UserReviewManage.js",
            "/Modules/ReviewManagement/Language/AspxReviewManagement.js");
    }
    protected void Page_Init(object sender, EventArgs e)
    {
        try
        {
            InitializeJS();
         }
        catch (Exception ex)
        {
            ProcessException(ex);
         }
     }
    private void InitializeJS()
    {
        Page.ClientScript.RegisterClientScriptInclude("metadata", ResolveUrl("~/js/StarRating/jquery.MetaData.js"));
        Page.ClientScript.RegisterClientScriptInclude("JTablesorter", ResolveUrl("~/js/GridView/jquery.tablesorter.js"));
    }


}