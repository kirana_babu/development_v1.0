﻿var arrItemListType = new Array();
var arrResultToBind = new Array();
var itemTemplateViewScriptArr = [];
/***********************Below section is added by aniket for Cost varients*****************************/
var variantId = new Array();
var arrCombination = [];
var SKU = '';
var ItemId = '';

/***********************End*****************************/

IsExistedCategory = function (arr, cat) {
    var isExist = false;
    for (var i = 0; i < arr.length; i++) {
        if (arr[i] == cat) {
            isExist = true;
            break;
        }
    }
    return isExist;
};
Array.prototype.clean = function (deleteValue) {
    for (var i = 0; i < this.length; i++) {
        if (this[i] == deleteValue) {
            this.splice(i, 1);
            i--;
        }
    }
    return this;
};

function GetItemTemplateScipt(key) {
    //debugger;
    var val = "";
    if (itemTemplateViewScriptArr.length > 0) {

        for (var i = 0; i < itemTemplateViewScriptArr.length; i++) {
            if (itemTemplateViewScriptArr[i].TemplateKey == key) {
                val = itemTemplateViewScriptArr[i].TemplateValue;
                break;
            }
        }
    }
    return val;
}

function GridView(appendDiv, allowAddToCart, allowOutStockPurchase, noImagePathDetail, noOfItemsInRow) {
    //debugger;
    $("#" + appendDiv).html('');
    var itemIds = [];
    if (window.location.href.indexOf("brand") > -1) {
        var tempScriptGridView = GetItemTemplateScipt('scriptResultBrandGrid');
    }
    else {
        var tempScriptGridView = GetItemTemplateScipt('scriptResultGrid');
    }
    var CostVariants = '';
    $.each(arrResultToBind, function (index, value) {
        //debugger;
        if (!IsExistedCategory(itemIds, value.ItemID)) {
            itemIds.push(value.ItemID);
            var imagePath = itemImagePath + value.BaseImage;
            if (value.BaseImage == "") {
                imagePath = noImagePathDetail;
            }
            else {
                imagePath = imagePath.replace('uploads', 'uploads/Medium')

            }
            var name = '';
            if (value.Name.length > 50) {
                name = value.Name.substring(0, 50);
                var i = 0;
                i = name.lastIndexOf(' ');
                name = name.substring(0, i);
                name = name + "...";
            } else {
                name = value.Name;
            }
            var items = [{
                itemID: value.ItemID,
                name: name,
                titleName: value.Name,
                SanchiCommerceRoot: aspxRedirectPath,
                sku: value.SKU,
                imagePath: aspxRootPath + imagePath,
                // loaderpath: SanchiCommerce.utils.GetAspxTemplateFolderPath() + "/images/loader_100x12.gif",
                alternateText: value.Name,
                listPrice: value.ListPrice,
                price: value.Price,
                isCostVariant: '"' + value.IsCostVariantItem + '"',
                shortDescription: Encoder.htmlDecode(value.ShortDescription),
                ItemTypeID: value.ItemTypeID,
                Quantity: value.Quantity
            }];
            // $.tmpl("scriptResultGridTemp", items).appendTo("#" + appendDiv);
            $.tmpl(tempScriptGridView, items).appendTo("#" + appendDiv);
            // to bind dynamic attribute'

            if (value.AttributeValues != null) {
                if (value.AttributeValues != "") {
                    var attrHtml = '';
                    attrValues = [];
                    attrValues = value.AttributeValues.split(',');
                    attrHtml = "<div class='cssGridDyanamicAttr'>";
                    for (var i = 0; i < attrValues.length; i++) {
                        var attributes = [];
                        attributes = attrValues[i].split('#');
                        var attributeName = attributes[0];
                        var attributeValue = attributes[1];
                        var inputType = parseInt(attributes[2]);
                        var validationType = attributes[3];
                        attrHtml += "<div class=\"cssDynamicAttributes\">";
                        attrHtml += "<span>";
                        attrHtml += attributeName;
                        attrHtml += "</span> :";
                        if (inputType == 7) {
                            attrHtml += "<span class=\"cssClassFormatCurrency\">";
                        }
                        else {
                            attrHtml += "<span>";
                        }
                        attrHtml += attributeValue;
                        attrHtml += "</span></div>";
                    }
                    attrHtml += "</div>";
                    $('.cssGridDyanamicAttr').html(attrHtml);
                }
                else {
                    $('.cssGridDyanamicAttr').remove();
                }
            }
            else {
                $('.cssGridDyanamicAttr').remove();
            }

            if (value.IsCostVariantItem == true) {
                //debugger;
                var indx = value.CostVariants.split('#').length;
                var data = value.CostVariants.split('#');
                var myJsonString = $.extend({}, data);
                if (BrandItemList){
                    BrandItemList.BindCostVariant(myJsonString, value.SKU, value.ItemID);
                }
                else 
                if (categoryDetails)
                   {categoryDetails.BindCostVariant(myJsonString, value.SKU, value.ItemID);}
            }
            //   $('.cssClassFormatCurrency').formatCurrency({ colorize: true, region: '' + region + '' });
            if (value.ListPrice == "") {
                $(".cssRegularPrice_" + value.ItemID + "").remove();
            }

            if (allowAddToCart) {
                if (allowOutStockPurchase.toLowerCase() == 'false') {
                    if (value.IsOutOfStock) {
                        // $(".cssClassAddtoCard_" + value.ItemID + " span").html(getLocale(AspxTemplateLocale, 'Out Of Stock'));
                        // $(".cssClassAddtoCard_" + value.ItemID).removeClass('cssClassAddtoCard');
                        // $(".cssClassAddtoCard_" + value.ItemID).addClass('cssClassOutOfStock');
                        // $(".cssClassAddtoCard_" + value.ItemID + " a").removeAttr('onclick');

                        /*Added or Updated by Aniket for Cost Varients*/
                        $("#spanAvailability_" + value.ItemID + "").addClass('cssOutOfStock');
                        $("#spanAvailability_" + value.ItemID + "").html('<b>' + getLocale(DetailsBrowse, 'Out Of Stock') + '</b>');
                        $(".cssClassAddtoCard_" + value.ItemID + " .sfButtonwrapper span").html(getLocale(DetailsBrowse, 'Out Of Stock'));
                        $(".cssClassAddtoCard_" + value.ItemID + " .sfButtonwrapper label").css("display", "inline-block");
                        $(".cssClassAddtoCard_" + value.ItemID + " .sfButtonwrapper label span").css("color", "#ff3c00");
                        //$(".cssClassAddtoCard_" + value.ItemID).removeClass('cssClassAddtoCard');
                        $(".cssClassAddtoCard_" + value.ItemID + " .sfButtonwrapper").addClass('cssClassOutOfStockcolor');
                        $(".cssClassAddtoCard_" + value.ItemID).find('label').removeClass('i-cart cssClassGreenBtn');
                        $(".cssClassAddtoCard_" + value.ItemID + " a").removeAttr('onclick');
                        /*end by Aniket for Cost Varients*/

                        //$(".cssClassAddtoCard_" + value.ItemID + " span").html(getLocale(AspxTemplateLocale, 'Out Of Stock'));
                        //$(".cssClassAddtoCard_" + value.ItemID).find("div").addClass("cssClassOutOfStock");
                        //$(".cssClassAddtoCard_" + value.ItemID).find('label').removeClass('i-cart cssClassGreenBtn');
                        //$(".cssClassAddtoCard_" + value.ItemID + "button").removeAttr("onclick");
                    }
                    else { $("#spanAvailability_" + value.ItemID + "").addClass('cssInStock'); $("#spanAvailability_" + value.ItemID + "").html('<b>' + getLocale(DetailsBrowse, 'In stock') + '</b>'); }
                }
            }
            else { $(".cssClassAddtoCard_" + value.ItemID).hide(); }
            if (value.ItemTypeID == 5) {
                $(".cssClassCompareAttributeClass >span").each(function () {
                    if ($(this).html() == "Is In Stock: ") {
                        $(this).parents("tr").find("td").eq(index + 1).html("No");
                    }
                });
                $(".cssClassAddtoCard_" + value.ItemID + "").parents(".cssLatestItemInfo").find(".cssClassProductRealPrice").prepend(getLocale(AspxTemplateLocale, "Starting At"));
            }
        }

    });
   // $(".cssClassDisplayResult").wrapInner("<div class='cssProductGridWrapper'></div>");
    //var x = 0;
    //var divtags = $('#divShowCategoryItemsList .cssClassProductsBox ');
    //var htmlStr = "";
    //var parentDiv = "";
    //var wrapAllIdenfier = 1;

    //$('#' + appendDiv + ' .cssClassProductsBox ').each(function () {
    //    x++;

    //    $(this).attr('xid', x);
    //    if ((x % 3) == 0) {
    //        $(this).addClass('cssClassNoMargin').addClass('i' + wrapAllIdenfier);
    //        $('.i' + wrapAllIdenfier).wrapAll("<div class='cssCatProductGridWrapper'></div>");
    //        wrapAllIdenfier++;
    //    }
    //    else $(this).addClass('i' + wrapAllIdenfier)
    //});


    //code is added for product box bottom 0 by aniket
    var x = 0;
    var divtags = $('#divBrandSearchResult .cssClassProductsBox ');
    var htmlStr = "";
    var parentDiv = "";
    var wrapAllIdenfier = 1;

    $('#divBrandSearchResult .cssClassProductsBox ').each(function () {
        x++;
        if ((x % 3) == 0) {
            // $(this).addClass('cssClassNoMargin');
            $(this).addClass('cssClassNoMargin').addClass('i' + wrapAllIdenfier);
            $('.i' + wrapAllIdenfier).wrapAll("<div class='cssProductGridWrapper'></div>");
            wrapAllIdenfier++;
        }
        else $(this).addClass('i' + wrapAllIdenfier)
    });

    if (itemIds.length > $('.cssProductGridWrapper').length * 3) { $('.i' + wrapAllIdenfier).wrapAll("<div class='cssProductGridWrapper'></div>"); };
    //end

    var $container = $(".cssProductGridWrapper");
    $container.imagesLoaded(function () {
        $container.masonry({
            itemSelector: '.cssClassProductsBox',
            EnableSorting: false
        });
    });
    $('.cssClassProductPicture a img[title]').tipsy({ gravity: 'n' });
}

function ListView(appendDiv, allowAddToCart, allowOutStockPurchase, noImagePathDetail, noOfItemsInRow) {
    //debugger;
    $("#" + appendDiv).html('');
    var itemIds = [];
    if (window.location.href.indexOf("brand") > -1) {
        var tempScriptListView = GetItemTemplateScipt('scriptResultBrandList');
    }
    else {
        var tempScriptListView = GetItemTemplateScipt('scriptResultList');
    }
   
    $.each(arrResultToBind, function (index, value) {
        if (!IsExistedCategory(itemIds, value.ItemID)) {
            itemIds.push(value.ItemID);
            var imagePath = itemImagePath + value.BaseImage;
            if (value.BaseImage == "") {
                imagePath = noImagePathDetail;
            }
            else {
                imagePath = imagePath.replace('uploads', 'uploads/Medium')
            }
            var name = '';
            if (value.Name.length > 50) {
                name = value.Name.substring(0, 50);
                var i = 0;
                i = name.lastIndexOf(' ');
                name = name.substring(0, i);
                name = name + "...";
            } else {
                name = value.Name;
            }
            var items = [{
                itemID: value.ItemID,
                name: name,
                titleName: value.Name,
                SanchiCommerceRoot: aspxRedirectPath,
                sku: value.SKU,
                imagePath: aspxRootPath + imagePath,
                // loaderpath: SanchiCommerce.utils.GetAspxTemplateFolderPath() + "/images/loader_100x12.gif",
                alternateText: value.Name,
                listPrice: value.ListPrice,
                price: value.Price,
                isCostVariant: '"' + value.IsCostVariantItem + '"',
                shortDescription: Encoder.htmlDecode(value.ShortDescription),
                ItemTypeID: value.ItemTypeID,
                Quantity: value.Quantity
            }];

            //$.tmpl("scriptResultListTemp", items).appendTo("#" + appendDiv);
            $.tmpl(tempScriptListView, items).appendTo("#" + appendDiv);

            // to bind dynamic attribute
            if (value.AttributeValues != null) {
                if (value.AttributeValues != "") {
                    var attrHtml = '';
                    attrValues = [];
                    attrValues = value.AttributeValues.split(',');
                    for (var i = 0; i < attrValues.length; i++) {
                        var attributes = [];
                        attributes = attrValues[i].split('#');
                        var attributeName = attributes[0];
                        var attributeValue = attributes[1];
                        var inputType = parseInt(attributes[2]);
                        var validationType = attributes[3];
                        attrHtml += "<div class=\"cssDynamicAttributes\">";
                        if (inputType == 7) {
                            attrHtml += "<span class=\"cssClassFormatCurrency\">";
                        }
                        else {
                            attrHtml += "<span>";
                        }
                        attrHtml += attributeValue;
                        attrHtml += "</span></div>";
                    }
                    $('.cssListDyanamicAttr').html(attrHtml);
                }
                else {
                    $('.cssListDyanamicAttr').remove();
                }
                if (value.IsCostVariantItem) {
                    var indx = value.CostVariants.split('#').length;
                    var data = value.CostVariants.split('#');
                    var myJsonString = $.extend({}, data);
                    //categoryDetails.BindCostVariant(myJsonString, value.SKU, value.ItemID);
                    if (BrandItemList) {
                        BrandItemList.BindCostVariant(myJsonString, value.SKU, value.ItemID);
                    }
                    else if (categoryDetails)
                    { categoryDetails.BindCostVariant(myJsonString, value.SKU, value.ItemID); }
                }
            }
            else {
                $('.cssListDyanamicAttr').remove();
            }

            if (value.ListPrice == "") {
                $(".cssRegularPrice_" + value.ItemID + "").remove();
            }
            if (allowAddToCart.toLowerCase() == 'true') {
                if (allowOutStockPurchase.toLowerCase() == 'false') {
                    if (value.IsOutOfStock) {
                        //$(".cssClassAddtoCard_" + value.ItemID + " span").html(getLocale(AspxTemplateLocale, 'Out Of Stock'));
                        //$(".cssClassAddtoCard_" + value.ItemID).find("div").addClass("cssClassOutOfStock");
                        //$(".cssClassAddtoCard_" + value.ItemID).find('label').removeClass('i-cart cssClassGreenBtn');
                        //$(".cssClassAddtoCard_" + value.ItemID + "button").removeAttr("onclick");

                        $("#spanAvailability_" + value.ItemID + "").addClass('cssOutOfStock');
                        $("#spanAvailability_" + value.ItemID + "").html('<b>' + getLocale(DetailsBrowse, 'Out Of Stock') + '</b>');
                        $(".cssClassAddtoCard_" + value.ItemID + " .sfButtonwrapper span").html('Out Of Stock');
                        //$(".cssClassAddtoCard_" + value.ItemID).removeClass('cssClassAddtoCard');
                        $(".cssClassAddtoCard_" + value.ItemID + " .sfButtonwrapper").addClass('cssClassOutOfStockcolor');
                        $(".cssClassAddtoCard_" + value.ItemID + " .sfButtonwrapper label").css("display", "inline-block");
                        $(".cssClassAddtoCard_" + value.ItemID + " .sfButtonwrapper label span").css("color", "#ff3c00");
                        $(".cssClassAddtoCard_" + value.ItemID).find('label').removeClass('i-cart cssClassGreenBtn');
                        $(".cssClassAddtoCard_" + value.ItemID + " a").removeAttr('onclick');
                    }
                    else { $("#spanAvailability_" + value.ItemID + "").addClass('cssInStock'); $("#spanAvailability_" + value.ItemID + "").html('<b>' + getLocale(DetailsBrowse, 'In stock') + '</b>'); }
                }
            }
            else { $(".cssClassAddtoCard_" + value.ItemID).hide(); }
            if (value.ItemTypeID == 5) {
                $(".cssClassAddtoCard_" + value.ItemID + "").parents(".cssLatestItemInfo").find(".cssClassProductRealPrice").prepend(getLocale(AspxTemplateLocale, "Starting At"));
            }
        }
    });
    $('.cssClassProductPicture a img[title]').tipsy({ gravity: 'n' });
}

function BindResults(appendDiv, divViewAs, ddlViewAs, mainVar, allowAddToCart, allowOutStockPurchase, noImagePathDetail, noOfItemsInRow, displayMode) {

    var viewAsOption = '';
    if (displayMode == "icon") {
        viewAsOption = $("#" + divViewAs).find('a.sfactive').attr("displayId");
        if (typeof viewAsOption == 'undefined') {
            $("#" + divViewAs).find('a:eq(0)').addClass("sfactive");
            viewAsOption = $("#" + divViewAs).find('a.sfactive').attr("displayId");
        }
    }
    else {
        viewAsOption = $("#" + ddlViewAs).val();
    }
    if (arrResultToBind.length > 0) {

        switch (viewAsOption) {
            case '1':
                GridView(appendDiv, allowAddToCart, allowOutStockPurchase, noImagePathDetail, noOfItemsInRow);
                break;
            case '2':
                ListView(appendDiv, allowAddToCart, allowOutStockPurchase, noImagePathDetail, noOfItemsInRow);
                break;
        }
        if (displayMode == "dropdown") {
            $("#" + ddlViewAs).val(viewAsOption);
        }
    }
    var cookieCurrency = $("#ddlCurrency").val();
    Currency.currentCurrency = BaseCurrency;
    Currency.convertAll(Currency.currentCurrency, cookieCurrency);
}

function BindTemplateDetails(appendDiv, viewOptionDiv, divViewAs, ddlViewAs, ddlSortBy, divSearchPageNumber, divPagination, ddlPageSize, currentPage, msg, varFunctionName, mainVar, allowAddToCart, allowOutStockPurchase, noImagePathDetail, noOfItemsInRow, displayMode, templateArray) {

    var rowTotal = 0;
    itemTemplateViewScriptArr = templateArray;
    $("#" + appendDiv).show();
    arrItemListType.length = 0;
    var length = msg.d.length;
    if (length > 0) {
        $("#" + appendDiv).html('');
        $("#" + viewOptionDiv).show();
        $("#" + divSearchPageNumber).show();
        $("#" + divViewAs).val(1);
        var itemIds = [];
        var headerElements = '';
        var imgCount = 0;
        var value;

        for (var index = 0; index < length; index++) {
            value = msg.d[index];
            rowTotal = value.RowTotal;
            if (value.ItemID != null) {
                if (!IsExistedCategory(itemIds, value.ItemID)) {
                    itemIds.push(value.ItemID);
                    arrItemListType.push(value);
                }
            }

        };
        if (arrItemListType.length > 0) {
            var items_per_page = $('#' + ddlPageSize).val();
            $("#" + divPagination).pagination(rowTotal, {
                // callback: categoryDetails.pageselectCallback,
                items_per_page: items_per_page,
                //num_display_entries: 10,
                current_page: currentPage,
                callfunction: true,
                function_name: { name: varFunctionName, limit: $('#' + ddlPageSize).val(), sortBy: $('#' + ddlSortBy).val() },
                prev_text: "Prev",
                next_text: "Next",
                prev_show_always: false,
                next_show_always: false
            });

            var max_elem = arrItemListType.length;
            arrResultToBind.length = 0;
            // Iterate through a selection of the content and build an HTML string
            for (var i = 0; i < max_elem; i++) {
                arrResultToBind.push(arrItemListType[i]);
            }

            //BIND FUN
            BindResults(appendDiv, divViewAs, ddlViewAs, mainVar, allowAddToCart, allowOutStockPurchase, noImagePathDetail, noOfItemsInRow, displayMode);
            $("#" + viewOptionDiv).show();
            $("#" + divPagination).show();
        } else {
            $("#" + viewOptionDiv).hide();
            $("#" + divSearchPageNumber).hide();
            $("#" + appendDiv).html("<span class='cssClassNotFound'>" + getLocale(CoreJsLanguage, "No items found!") + "</span>");
        }
    }
    else {
        $("#" + viewOptionDiv).hide();
        $("#" + divSearchPageNumber).hide();
        $("#" + appendDiv).html("<span class='cssClassNotFound'>" + getLocale(CoreJsLanguage, "No items found!") + "</span>");
    }
}

function CreateDdlPageSizeOption(dropDownId) {
    $("#" + dropDownId).html('');
    var optionalSearchPageSize = '';
    optionalSearchPageSize += "<option data-html-text='9' value='9'>" + 9 + "</option>";
    optionalSearchPageSize += "<option data-html-text='18' value='18'>" + 18 + "</option>";
    optionalSearchPageSize += "<option data-html-text='27' value='27'>" + 27 + "</option>";
    optionalSearchPageSize += "<option data-html-text='36' value='36'>" + 36 + "</option>";
    optionalSearchPageSize += "<option data-html-text='45' value='45'>" + 45 + "</option>";
    optionalSearchPageSize += "<option data-html-text='90' value='90'>" + 90 + "</option>";

    //optionalSearchPageSize += "<option data-html-text='8' value='8'>" + 8 + "</option>";
    //optionalSearchPageSize += "<option data-html-text='16' value='16'>" + 16 + "</option>";
    //optionalSearchPageSize += "<option data-html-text='24' value='24'>" + 24 + "</option>";
    //optionalSearchPageSize += "<option data-html-text='32' value='32'>" + 32 + "</option>";
    //optionalSearchPageSize += "<option data-html-text='40' value='40'>" + 40 + "</option>";
    //optionalSearchPageSize += "<option data-html-text='64' value='64'>" + 64 + "</option>";
    $("#" + dropDownId).html(optionalSearchPageSize);
}

function createDropDown(ddlDropdown, divViewAs, option, displayMode) {
    var templateView = '';
    if (option.toLowerCase() == 'sortby') {
        $("#" + ddlDropdown).html('');
        if (sortByOptions != "") {
            var sortByOption = sortByOptions.split(',').clean('');
            $.each(sortByOption, function (i) {
                var sortByOption1 = sortByOption[i].split('#');
                var displayOptions = "<option data-html-text='" + sortByOption1[1] + "' value=" + sortByOption1[0] + ">" + sortByOption1[1] + "</option>";
                $("#" + ddlDropdown).append(displayOptions);
            });
            $("#" + ddlDropdown).val(sortByOptionsDefault);
            $("#" + divViewAs).show(); //TO BE REMOVED}
        }
    }
    else if (option.toLowerCase() == 'viewas') {
        if (displayMode.toLowerCase() == "dropdown") {
            $("#" + ddlDropdown).html('');
            if (viewAsOptions != "") {
                var viewAsOption = viewAsOptions.split(',').clean('');
                $.each(viewAsOption, function (i) {
                    var viewAsOption1 = viewAsOption[i].split('#');
                    var displayOptions = "<option value=" + viewAsOption1[0] + ">" + viewAsOption1[1] + "</option>";
                    $("#" + ddlDropdown).append(displayOptions);
                });
                $("#" + ddlDropdown).val(viewAsOptionsDefault);
                $("#" + ddlDropdown).show();
                $("#" + divViewAs).show(); //to be removed
                //$("#ddlSortBy").MakeFancyItemDropDown();
                //    categoryDetails.LoadAllCategoryContents(1, parseInt($("#ddlPageSize").val()), 0, $("#ddlSortBy option:selected").val());
            }
        }
        else if (displayMode.toLowerCase() == "icon") {
            $("#" + divViewAs).find('h4:first').remove();
            if (viewAsOptions != "") {
                var viewAsOption = viewAsOptions.split(',').clean('');
                $.each(viewAsOption, function (i) {
                    var viewAsOption1 = viewAsOption[i].split('#');
                    var displayOptions = "<a class='cssClass" + viewAsOption1[1] + " i-" + viewAsOption1[1] + "' id='view_" + viewAsOption1[0] + "' displayId='" + viewAsOption1[0] + "'   title=" + viewAsOption1[1] + "></a>";
                    $("#" + divViewAs).append(displayOptions);
                });
                //$('#' + templateView.vars.divSort + 'a[title]').tipsy({ gravity: 'n' });
                $("#" + divViewAs).find('a').each(function (i) {
                    if ($(this).attr('displayId') == viewAsOptionsDefault) {
                        $(this).addClass('sfactive');
                    }
                });
                $("#" + divViewAs).show();
                $("#" + divViewAs + " a[title]").tipsy({ gravity: 'n' });
            }

        }
    }
}
