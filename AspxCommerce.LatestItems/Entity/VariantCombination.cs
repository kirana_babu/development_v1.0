﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SanchiCommerce.LatestItems.Entity
{
    public class VariantCombination
    {
        public VariantCombination()
        {
        }


        public int CombinationID { get; set; }
        public int CombinationValues { get; set; }
        public float CombinationPriceModifier { get; set; }
        public float CombinationWeightModifier { get; set; }
        public int CombinationPriceModifierType { get; set; }
        public int CombinationWeightModifierType { get; set; }
        public int CombinationQuantity { get; set; }
        public string ImageFile { get; set; }
        public int CombinationType { get; set; }

    }
}
